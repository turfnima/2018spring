
///////////////////////////////////////////////////////
// helixmod.cpp
//
// This program approximates a helix with a line strip.
//
// Sumanta Guha.
//
// modified for side view
// interactions:  
//       s toggles top view and side view
///////////////////////////////////////////////////////        

#include <cstdlib>
#include <cmath>
#include <iostream>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

#define PI 3.14159265

using namespace std;

//global
bool yzSwap=false;

// Drawing routine.
void drawScene(void)
{  
   float R = 20.0; // Radius of helix.

   float t; // Angle parameter.

   glClear(GL_COLOR_BUFFER_BIT);
   glColor3f(0.0, 0.0, 0.0);
  if (!yzSwap)
  {
    
  
   glBegin(GL_LINE_STRIP);
      for(t = -10 * PI; t <= 10 * PI; t += PI/20.0) 
         glVertex3f(R * cos(t), R * sin(t), t - 60.0);
   glEnd();
}
  else {
    
  

  glBegin(GL_LINE_STRIP);
  for(t = -10 * PI; t <= 10 * PI; t += PI/20.0) 
    glVertex3f(R * cos(t), t - 0.0,R * sin(t)-50.0);
  glEnd();
  }
   glFlush();
}

// Initialization routine.
void setup(void) 
{
   glClearColor(1.0, 1.0, 1.0, 0.0); 
  glLineWidth(2.0);
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
   glViewport(0, 0, (GLsizei)w, (GLsizei)h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(-50.0, 50.0, -50.0, 50.0, 0.0, 100.0);
    //glFrustum();  //mjb
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
   switch(key) 
   {
      case 27:
         exit(0);
         break;
     case 's':  //for swap view
       yzSwap=!yzSwap;
       glutPostRedisplay();
      default:
         break;
   }
}

// Routine to output interaction instructions to the C++ window.
void printInteraction(void)
{
  cout << "Interaction:" << endl;
  cout << "Press s to toggle top and side view of helix." << endl;  
}

// Main routine.
int main(int argc, char **argv) 
{
  printInteraction();
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); 
   glutInitWindowSize(500, 500);
   glutInitWindowPosition(100, 100); 
   glutCreateWindow("helixmod.cpp");
   setup(); 
   glutDisplayFunc(drawScene); 
   glutReshapeFunc(resize);  
   glutKeyboardFunc(keyInput);
   glutMainLoop(); 

   return 0;  
}
