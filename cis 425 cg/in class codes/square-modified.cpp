////////////////////////////////////////////////////
// square.cpp
//
// Stripped down OpenGL program that draws a square.
//
// by Sumanta Guha. and is originally downloaded from her website

//modified by Xiaozhi Li for cis 425 class.
////////////////////////////////////////////////////
//some notes about function prototype:
// if the body of the function was above the main(),
//there doesnt need to have a prototype function.

#include <iostream>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

using namespace std;

// Drawing (display) routine.
void drawScene(void)
{
   // Clear screen to background color.
   glClear(GL_COLOR_BUFFER_BIT);

   // Set foreground (or drawing) color.
   glColor3f(0.0, 0.0, 0.0);

   // Draw a polygon with specified vertices.
   glBegin(GL_POLYGON);
      glVertex3f(20.0, 20.0, 0.0);
      glVertex3f(80.0, 20.0, 0.0);
      glVertex3f(80.0, 80.0, 0.0);
      glVertex3f(20.0, 80.0, 0.0);
   glEnd();

   // Flush created objects to the screen, i.e., force rendering.
   glFlush();
}

// Initialization routine.
void setup(void)
{
   // Set background (or clearing) color.
   glClearColor(1.0, 1.0, 1.0, 0.0);
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
   // Set viewport size to be entire OpenGL window.
   glViewport(0, 0, (GLsizei)w, (GLsizei)h);

   // Set matrix mode to projection.
   glMatrixMode(GL_PROJECTION);

   // Clear current projection matrix to identity.
   glLoadIdentity();

   // Specify the orthographic (or perpendicular) projection,
   // i.e., define the viewing box.
   glOrtho(0.0, 100.0, 0.0, 100.0, -1.0, 1.0);

   // Set matrix mode to modelview.
   glMatrixMode(GL_MODELVIEW);

   // Clear current modelview matrix to identity.
   glLoadIdentity();
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
   switch(key)
   {
	  // Press escape to exit.
      case 27:
         exit(0);
         break;
      default:
         break;
   }
}

// Main routine: defines window properties, creates window,
// registers callback routines and begins processing.
int main(int argc, char **argv)
/*we can ignore the (int argc, char **argv), but we have to have it*/
{
   // Initialize GLUT.
   glutInit(&argc, argv);

   // Set display mode as single-buffered and RGB color.
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

   // Set OpenGL window size.
   //dimension of the window
   glutInitWindowSize(500, 500);

   // Set position of OpenGL window upper-left corner.
   //upper-left 0,0 x goes from left to right
   //y goes from top to down.
   glutInitWindowPosition(100, 100);

   // Create OpenGL window with title.
   glutCreateWindow("square.cpp");

   // Initialize.
   setup();
   //wrote for clear color

   // Register display routine.
   glutDisplayFunc(drawScene);
   //takes a function as parameter
   // drawScene has to have void type
   //and has to have no parameters.

   // Register reshape routine.
   glutReshapeFunc(resize);

   // Register keyboard routine.
   glutKeyboardFunc(keyInput);

   // Begin processing.
   glutMainLoop();
   //this function is waiting for something to happen.(beggining of the action)
   //has more things to do with a machine

   return 0;
}
