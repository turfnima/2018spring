/******************************************
*
* Official Name:  Xiaozhi LI
*
* Nickname:  Caesar
*
* E-mail:  xli137@syr.edu
*
* Assignment:  Assignment 03
*
* Environment/Compiler:  Visual Studio 2015
*
* Date:  February 26, 2018
*
* References:  
*
* Interactions:   

features:
top view
*******************************************/

//normal logistic
#include <cmath>
#include <iostream>
//vector for vector
#include <cstdlib>
#include <vector>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

#define PI 3.14159265
using namespace std;

//window size and positions
static double window_w = 640;
static double window_h = 360;
static double window_position_x = 100;
static double window_position_y = 200;

//view box sizes
static double view_w = 40;
static double view_h = 40;
static double view_d = 30;


//cordinates for the eye
static double cam_move_z=35;
static double cam_rotate_y = 0.0;
static int zoomOption = 0;
static double camZ = -300.0;

//the location of the bug
static double bug_x = 0;
static double bug_y = 0;
static double bug_z = 0;

static double fly_x = 0;
static double fly_y = 0;
static double fly_z = 0;

static double fly_angle = 0.0;

//the size of the bug
static double bug_size = 3;
static double wing_angle = -10;

//size of the flowr
static GLdouble flower[4][3];


//flip degree:
static double degree = 3;
static bool isUp = false;


//switching states:
static int topOrFront = 0; //if 0 front view, if 1 top view
static bool isWingFlipping = false;
static bool isAnimate = false;
static int animationPeriod = 30; // Time interval between frames.
static double someT = 0;
static int targetFlower = 0;
//closest hit
//from BallAndTorusPicking
int closestName;
static unsigned int buffer[1024]; // Hit buffer.
static int hits; // Number of entries in hit buffer.
bool isSelecting = false;
bool isDragging = false;

//if flowerShape==0 circular flowers,
//else square flowers
static int flowerShape = 1;
static double flowerP = 15;
//prototypes:
void drawEye(GLdouble angle);
void drawHead();
void useless();
void drawNose(GLdouble base, GLdouble h);
void drawWing(GLdouble length, GLdouble angle);
void drawBody();
void wingFlip();
void reset();
void drawPetal(GLdouble angle);
void drawPistril();
//void drawEverything();
//void setProjection();
// Process hit buffer to find record with smallest min-z value.
 //Copied (almost) exactly from BallAndTorusPicking
//void findClosestHit(int hits, unsigned int buffer[])
//{
//	unsigned int *ptr, minZ;
//
//	minZ = 0xffffffff; // 2^32 - 1
//	ptr = buffer;
//	closestName = 0;
//	for (int i = 0; i < hits; i++)
//	{
//		ptr++;
//		if (*ptr < minZ)
//		{
//			minZ = *ptr;
//			ptr += 2;
//			closestName = *ptr;
//			ptr++;
//		}
//		else ptr += 3;
//	}
//} // end findClosestHit


//from inclass code
//void setProjection()
//{
//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//	glOrtho(-view_w, view_w, -view_h, view_h, -view_d, view_d);
//	glMatrixMode(GL_MODELVIEW);
//} // end setProjection

//void setPointOfView()
//{	glMatrixMode(GL_MODELVIEW);
//	glLoadIdentity();
//} // end setPointOfView

// The mouse callback routine.
//based on BallAndTorusPicking pickFunction
//void mousePickFunction(int button, int state, int x, int y)
//{
//	int viewport[4]; // Viewport data.
//
//	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) //mouse clicked
//	{
//		//**
//		//cout << "left button pressed" << endl;
//		glGetIntegerv(GL_VIEWPORT, viewport); // Get viewport data.
//		glSelectBuffer(1024, buffer); // Specify buffer to write hit records in selection mode
//		(void)glRenderMode(GL_SELECT); // Enter selection mode.
//
//									   //**************  Set new viewing volume ***********
//									   // Save the viewing volume defined in the setProjection routine,
//									   // the real projection matrix for rendering
//		glMatrixMode(GL_PROJECTION);
//		glPushMatrix();
//
//		// Define a viewing volume corresponding to selecting in
//		//    3 x 3 region around the cursor.
//		glLoadIdentity();
//		gluPickMatrix((float)x, (float)(viewport[3] - y), 5.0, 5.0, viewport);
//		//real projection used, copied from setProjection
//		//DON'T load identity here
//
//		glOrtho(-view_w, view_w, -view_h, view_h, -view_d, view_d);
//
//		//************** END of Set new viewing volume ***********
//
//		//---------------  Selecting --------------
//		//set up modelview for selecting
//		glMatrixMode(GL_MODELVIEW); // Return to modelview mode before drawing.
//		glLoadIdentity();  //I do this later, in setPointOfView
//
//		glInitNames(); // Initializes the name stack to empty.
//		glPushName(0); // Puts name 0 on top of stack.
//
//					   // Determine hits by drawing so that names are assigned.
//		isSelecting = 1;
//		drawEverything();  //ALL drawing must be done
//							//NO MESSING with projection matrix
//
//		hits = glRenderMode(GL_RENDER); // Return to rendering mode, returning number of hits.
//
//         								// Determine closest of the hit objects (if any).
//										// closest object name will be saved in closestName.
//		findClosestHit(hits, buffer);
//
//		//if red cube was hit, start dragging.
//		if (closestName == 1) //clicked on red cube
//			isDragging = true;
//		//**
//		cout << "closest hit = " << closestName << endl;
//		//---------------  END OF Selecting --------------
//
//		//back to drawing mode
//		// Restore viewing volume of the resize routine and return to modelview mode.
//		glMatrixMode(GL_PROJECTION);
//		glPopMatrix();
//		glMatrixMode(GL_MODELVIEW);
//
//		glutPostRedisplay();  //redraw for real, knowing what was hit.
//
//	} // end if left button pressed
//	if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) //mouse released
//	{
//		isDragging = false;
//	}
//}
//test draw
void useless() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	//have this bug up and down
	//glLoadIdentity();
	glPushMatrix();
	glTranslated(0, 1+bug_y, 0);
	//drawHead();
	drawHead();
	drawBody();
	glPopMatrix();
	glDisable(GL_DEPTH_TEST);
}
//draw body of the bug
void drawBody() {
	//draw a scaled sphere as body
	glPushMatrix();
	glRotated(60, 0, 0, -1);
	//draw wings on body
	glColor3f(0.0, 1.0, 0.0);
	drawWing(bug_size,wing_angle);
	glScalef(.3, 1, .3);
	glColor3f(0.8, 0.8, 0.0);
	glutSolidSphere(bug_size, 20, 20);
	glPopMatrix();
	
}

//draw the head
void drawHead() {
	//draw a sphere as head
	glPushMatrix();
	glRotated(60, 0, 0, -1);
	glTranslated(0.0, 0.8*bug_size, 0);
	//draw eyes on this sphere
	drawEye(30);
	drawEye(-30);
	//draw proboscis on head (i just call it nose)
	drawNose(0.2*0.4*bug_size, 0.4*bug_size);
	//the head
	//color of the head
	glColor3f(0.0, 1.0, 1.0);
	glutSolidSphere(0.4*bug_size, 20, 20);
	glPopMatrix();
}

//draw eyes
void drawEye(GLdouble angle) {
	//color of the eyes
	glColor3f(1.0, 0.0, 0.0);
	glPushMatrix();
	glRotated(angle, 1, 0, 0);
	glTranslated(0.0, 0.4*bug_size, 0);
	glScaled(1, 1.5, 1.5);
	glutSolidSphere(0.1*0.4*bug_size, 20, 20);
	glPopMatrix();
}
//draw wing
void drawWing(GLdouble length, GLdouble angle) {

	glPushMatrix();
	//where animation is due
	glRotated(angle, 0, 1, 0);
	glTranslated(0.0, 0.45 * bug_size, 0);
	glRotated(90, 0, -1, 0);
	glRotated(70, 0, 0, -1);
	
	glTranslated(0.30*bug_size, length*0.9, 0.30*length);
	glScaled(0.26, 1, 1);
	glBegin(GL_POLYGON);
	GLdouble t = 0.0;
	for (int i = 0; i < 40; i++) {
		glVertex3d(cos(t)*length, sin(t)*length, 0.0);
		glVertex3d(cos(t)*length, sin(t)*length, 0.1);
		t += 2 * PI / 40;
	}
	glEnd();
	glPopMatrix();

	glPushMatrix();
	//where animation is due
	glRotated(-angle, 0, 1, 0);
	glTranslated(0.0, 0.45 * bug_size, 0);
	glRotated(90, 0, -1, 0);
	glRotated(-70, 0, 0, -1);

	

	glTranslated(-0.30*bug_size, length*0.9, 0.30*length);
	glScaled(0.26, 1, 1);
	glBegin(GL_POLYGON);
	t = 0.0;
	for (int i = 0; i < 40; i++) {
		glVertex3d(cos(t)*length, sin(t)*length, 0.0);
		glVertex3d(cos(t)*length, sin(t)*length, 0.1);
		t += 2 * PI / 40;
	}
	glEnd();
	glPopMatrix();
}
//draw proboscis
void drawNose(GLdouble base, GLdouble h) {
	glColor3f(1.0, 0.0, 0.0);
	glPushMatrix();
	glRotated(60, 0, 0, -1);
	glTranslated(0.0, 0.4*bug_size, 0);	
	glRotated(90, -1, 0, 0);
	glutSolidCone(base,h,20,20);
	glPopMatrix();
}

//draw petal
void drawPetal(GLdouble angle) {
	
	glPushMatrix(); 
	glRotated(angle, 0, 1, 0);
	glTranslated(-0.22, 1.55, 0.0);
	glEnable(GL_CLIP_PLANE0);
	double clip0[4] = { -1, 0.0, 0.0, 0.0 };
	glClipPlane(GL_CLIP_PLANE0, clip0);
	glScaled(1.2,0.1,0.2);
	glColor3f(0.9, 0.9, 0.0);
	if (flowerShape == 0)
		glutSolidSphere(1.2, 30, 30);
	else
	{
		glColor3f(.7, .6, .3);
		glutSolidCube(2.3);
	}
	glPopMatrix();
	glDisable(GL_CLIP_PLANE0);
}

//draw pistril
void drawPistril() {
	glPushMatrix();
	
	glTranslated(0.0, 1.5, 0.0);
	glEnable(GL_CLIP_PLANE0);
	double clip0[4] = { 0.0, -1, 0.0, 0.0 };
	glClipPlane(GL_CLIP_PLANE0,clip0);
	glColor3f(0.75, 0.0, 0.0);
	glutWireSphere(.3, 15, 15);
	glDisable(GL_CLIP_PLANE0);
	glPopMatrix();
}

//draw flower
void drawFlower() {
	glPushMatrix();
	drawPetal(90);
	drawPetal(0);
	drawPetal(270);
	drawPetal(180);
	drawPistril();
	glScaled(0.05, 1.5, 0.05);
	glColor3f(0.0, 0.8, 0.0);
	glutSolidCube(2);
	glPopMatrix();
}

void drawManyFlower() {
	glClear(GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	//the flowers
	//flower no.0
	glPushMatrix();
	flowerShape = 0;
	glTranslated(flower[0][0], flower[0][1], flower[0][2]);
	drawFlower();
	glPopMatrix();
	//the flowers
	//flower no.1
	glPushMatrix();
	flowerShape = 1;
	glTranslated(flower[1][0], flower[1][1], flower[1][2]);
	drawFlower();
	glPopMatrix();
	//the flowers
	//flower no.2
	glPushMatrix();
	glTranslated(flower[2][0], flower[2][1], flower[2][2]);
	flowerShape = 0;
	drawFlower();
	glPopMatrix();


	//flower no.3
	glPushMatrix();
	glTranslated(flower[3][0], flower[3][1], flower[3][2]);
	flowerShape = 1;
	drawFlower();
	glPopMatrix();
	glDisable(GL_DEPTH_TEST);
}
//resize
void resize(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	//view box
	//glOrtho(-20,20,-20,20,-200,200);
	//if(topOrFront)
	gluPerspective(60.0, double(window_w)/double(window_h), 2, 100);
	//else {}
	//glOrtho(-view_w, view_w, -view_h, view_h, -view_d, view_d);
	//glFrustum(-25, 25, -25, 25, 1, 100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	reset();
}

//flip of the wing, the idle function
//idle
void wingFlip() {
	
	if (isUp && wing_angle<=45+degree)
	{
		wing_angle = wing_angle + degree;
		if (wing_angle >= 45) isUp = false;
	}
	else if (!isUp && wing_angle >=-20-degree)
	{
		wing_angle = wing_angle - degree;
		if (wing_angle <= -20) isUp = true;
	}
	glutPostRedisplay();
	
}

void levitating(void) {

	bug_y = 0.7 * sin(someT/30);
	//cout << "sin(someT/360)= " << sin(someT ) << endl;
	someT = someT + 3;
	if (someT/30 >= 360) someT = 0;
	glutPostRedisplay();
}

void bugRotate() {
	
}
//fly towards flowers
void flyToFlower() {
	switch(targetFlower){
	case 0:
		if(fly_x<flower[0][0])
		fly_x += 1;
		if (fly_z > flower[0][2])
		fly_z -= 1;
		if (fly_x == flower[0][0] && fly_z == flower[0][2])
			targetFlower = 1;
		//cout << "target flower= " << targetFlower<<endl;
		break;
	case 1:
		if (fly_x>flower[1][0])
			fly_x -= 1;
		if (fly_x == flower[1][0] && fly_z == flower[1][2])
			targetFlower = 2;
		break;
	case 2:
		if (fly_z < flower[2][2])
			fly_z += 1;
		if (fly_x == flower[2][0] && fly_z == flower[2][2])
			targetFlower = 3;
		break;
	case 3:
		if (fly_x < flower[3][0])
			fly_x += 1;
		if (fly_x == flower[3][0] && fly_z == flower[3][2])
		{
			targetFlower = 4;
			if (fly_angle > 180) fly_angle = 0;
		}
		break;
	case 4:
			if (fly_z > flower[0][2])
				fly_z -= 1;
			if (fly_x == flower[1][0] && fly_z == flower[1][2])
				targetFlower = 2;
			break;
	default:
		break;
	}
}
void animateFly(int someValue) {
	if (isAnimate &&targetFlower==0) {
		if (fly_angle <= 45)
		{	
			fly_angle += 3;
		}

		else flyToFlower();
	}
	else if (isAnimate &&targetFlower == 1)
	{
		if (fly_angle <= 180)
		{	
			fly_angle += 3;
		}
		else flyToFlower();
	}
	else if (isAnimate &&targetFlower == 2)
	{
		if (fly_angle <= 270)
		{	
			fly_angle += 3;
		}
		else flyToFlower();
	}
	else if (isAnimate &&targetFlower == 3)
	{
		if (fly_angle <= 360)
		{
			
			fly_angle += 3;
		}
		else
		{
			
			flyToFlower();
		}
	}
	else if (isAnimate &&targetFlower == 4)
	{
		if (fly_angle <= 90)
		{
			fly_angle += 3;
		}

		else flyToFlower();
	}
	glutTimerFunc(animationPeriod, animateFly, 1);
}
void animate(int someValue) {
	if (isAnimate)levitating();

	glutTimerFunc(animationPeriod,animate, 1);
}

//reset everything
void reset() {
	//resets my values
	fly_x = 0;
	fly_y = 0;
	fly_z = 0;
	fly_angle = 0.0;
	bug_x = 0;
	bug_y = 0;
	bug_z = 0;
	camZ = 0;
	targetFlower = 0;
	cam_move_z = 35;
	cam_rotate_y = 0.0;
	zoomOption = 0;
	camZ = -300.0;
	
	flower[0][0] = flowerP;
	flower[0][1] = 0.0;
	flower[0][2] = -flowerP;

	flower[1][0] = -flowerP;
	flower[1][1] = 0.0;
	flower[1][2] = -flowerP;

	flower[2][0] = -flowerP;
	flower[2][1] = 0.0;
	flower[2][2] = flowerP;

	flower[3][0] = flowerP;
	flower[3][1] = 0.0;
	flower[3][2] = flowerP;
	glutPostRedisplay();

}

//draw path


void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	case 's':
		
		break;

	case 'w':
		if(isWingFlipping && isAnimate){
			glutIdleFunc(NULL);
			isWingFlipping = false;
			isAnimate = false;
		}
		else {
			glutIdleFunc(wingFlip);
			isWingFlipping = true;
			isAnimate = true;
		}	
		break;
	case 'a':
		cam_rotate_y = cam_rotate_y + PI/72;
		glutPostRedisplay();
		break;
	case 'd':
		cam_rotate_y = cam_rotate_y - PI/72;
		glutPostRedisplay();
		break;
	case 'r':
		reset();
		glutPostRedisplay();
		break;
	case 't':
		//toggle to top view
		topOrFront = 1;
		
		glutPostRedisplay();
		break;
	case 'f':
		//toggle to front view
		topOrFront = 0;
		
		glutPostRedisplay();
			break;
	case'j':
		fly_x = fly_x - 1;
		glutPostRedisplay();
		break;
	case'l':
		fly_x = fly_x + 1;
		glutPostRedisplay();
		break;

	default:
		break;
	}
}

void printInteraction(void)
{
	cout << "Interaction:" << endl;

}
void drawScene() {

	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 1.0, 0.0);
	//glLoadIdentity is important for a static value.
	
	glLoadIdentity();
	if(topOrFront==0)
		gluLookAt(-cam_move_z*sin(cam_rotate_y),0, cam_move_z*cos(cam_rotate_y), 0,0,0,  0,1,0);
	//top view with rotating
	else if(topOrFront==1){
		gluLookAt(0, cam_move_z, 0,
			0, 0, 0,
			-sin(cam_rotate_y), 0, cos(cam_rotate_y));
	}
	
	

	//scene goes here
	//isSelecting = 0;
	//drawEverything();
	glPushMatrix();
	glTranslated(fly_x, fly_y, fly_z);
	//glRotated()
	glRotated(fly_angle,0,1,0);
	useless();
	glPopMatrix();
	drawManyFlower();
	//scene end
	//gl look at

	glutSwapBuffers();
	//notice this is for double buffer
}
//void drawEverything() {
//
//	setPointOfView();
//
//	//the bug
//	glPushMatrix();
//	if (isSelecting) glLoadName(1);
//	glTranslated(fly_x, fly_y, fly_z);
//	
//	useless();
//
//	glPopMatrix();
//	if (isSelecting) glLoadName(2);
//	drawManyFlower();
//	
//}
// Initialization routine.
void setup(void)
{
	glEnableClientState(GL_VERTEX_ARRAY);
  //blue background
	glClearColor(0, .7, 1, 0.0);
}

void mouseControl(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		cout << "x= " << x << endl;
		cout << "y= " << y << endl;
		//cout << "current angle= " << wing_angle << endl;
	}
}

void mouseMotion(int x, int y)
{
	if (isDragging)
	{
		double xsc, ysc, xobj, yobj;
		// convert to screen coords
		xsc = x;
		ysc = window_h - y;
		// convert to object coords
		xobj = xsc * (20.0 / window_w) - 10.0;
		yobj = ysc * (20.0 / window_h) - 10.0;

		fly_x = xobj;
		fly_y = yobj;

		glutPostRedisplay();
	} // end of dragging
} // end mouseMotion
// Main routine.
int main(int argc, char **argv)
{

	printInteraction();

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB| GLUT_DEPTH);
	//first window
	glutInitWindowSize(window_w, window_h);
	glutInitWindowPosition(window_position_x, window_position_y);

	//create the first window and return id
	glutCreateWindow("hw 03 animation");
	setup();

	//initialization, display and other routines.
	glutDisplayFunc(drawScene);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);
	glutMouseFunc(mouseControl);
	
	glutTimerFunc(5, animate, 1);
	glutTimerFunc(20, animateFly, 1);
	glutMainLoop();

	return 0;
}