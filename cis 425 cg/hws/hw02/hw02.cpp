
/******************************************
*
* Official Name:  Xiaozhi Li
*
* Nickname:  Caesar
*
* E-mail:  xli137@syr.edu
*
* Assignment:  Assignment 2
*
* Environment/Compiler:  Visual Studio 2017
*
* Date:  February 21, 2018
*
* References:  For constellation, I used my friend Ruizi Xu's design (line 312-332), I took his vertex coordinates, and coordinates only.

*
* Interactions:     
  cout << "press 'm' to toggle moon drag"<<endl;
  cout << "press 'j' to show Jupiter" << endl;
  cout << "press 's' to show big dipper" << endl;
  cout << "press 'd' toggle between day and night" << endl;
  cout<<"press 'p'to iterate from new moon to full moon"<<endl;
  cout << "press esc to quit" << endl;
  cout << "once toggled moon drag, you can click and drag moon to where ever you want." << endl;
*
what was special:
	the moon that moves in the display window follows the mouse, on a 1vs1 ratio.
	Changing the size of the window, the viewing box,(using the global values ) would not affect it.

*******************************************/
///////////////////////////////////////////////////////////////////////////////////////

//some prototype
void setup1(void);

//normal logistic
#include <cmath>
#include <iostream>
//vector for vector
#include <cstdlib>
#include <vector>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

#define PI 3.14159265

using namespace std;
//end of normal logistic

// Globals.
static double display_position_x = 500;
static double display_position_y = 350;
static double display_w = 500;
static double display_h = 500;

static double control_w = 400;
static double sky_rotate = 0;
static int toggle_moon_drag = 0;
//viewing box for display
//why do i do it this way: I can change viewing box however I want and the mousemotion will remain
static double view_w = 40;
static double view_h = 40;
static double view_d = 30;

//control panel start
static int radio_button = 0;

static int check_box_1 = 0;
static int check_box_2 = 0;
static int check_box_3 = 0;
static int check_box_4 = 0;

//control panel end

//right click menu
static int dayOrNight = 0;
static int moonScale = 0;
//right click menu end
//clip degrees:
static double a = 0;


static int id1, id2; //int for window id
//x,y,z, radius of moon
static double  moonR=9.0;
static double	moonX = 0.0;
static double	moonY = 10.0;
static double	moonZ = 0.0;
//console text
static long font = (long)GLUT_BITMAP_9_BY_15; // Font selection.
// Routine to draw a bitmap character string.
void writeBitmapString(void *font, char *string)
{
	char *c;
	for (c = string; *c != '\0'; c++) glutBitmapCharacter(font, *c);
}

//check box flipper
//if this check box value was 0, change it to 1, if it was 1, change it to 0
//if it is neither 0 nor 1, change it to 0
void checkBoxF(static int &cb) {
	if (cb == 0)
		cb = 1;
	else cb = 0;
	return;
}

//draw a moon (as sphere)
void drawMoon() {
	

	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 1.0, 0.0);// gl color
	//translate the ball to wanted corrdinate
	glPushMatrix();
	if (moonScale) glScalef(1.0, 1.4, 1.0);
	glPushMatrix();
	glTranslatef(moonX, moonY, moonZ);

	//here i use glRotatef to rotate the cliping plane
	glPushMatrix();
	glRotatef(a, 0.0, 1.0, 0.0);
	

	//cout << "angle is: " << a << endl;
	double clip0[4] = { 1, 0.0, 0.0, 0.0 };
	glEnable(GL_CLIP_PLANE0);
	glClipPlane(GL_CLIP_PLANE0, clip0);
	glPopMatrix();
	

	//glutWireSphere(3.0, 10, 10);
	glutSolidSphere(moonR, 20, 20);
	glDisable(GL_CLIP_PLANE0);

	//second moon
	glColor3f(.17, .17, 0.0);// gl color

	glPushMatrix();
	//glCLipplane goes in a different direction
	//I could use a second clip plane, but i just want to see what this does
	glRotatef(-a, 0.0, 1.0, 0.0);
	//cout << "angle is: " << a << endl;
	clip0[0] = -1;
	glEnable(GL_CLIP_PLANE0);
	glClipPlane(GL_CLIP_PLANE0, clip0);
	glPopMatrix();
	//somehow this sphere got to have twice the stacks
	//otherwise it wont cover the moon.
	
	glutSolidSphere(moonR, 20, 40);
	glDisable(GL_CLIP_PLANE0);
	//second moon ended
	glPopMatrix();
	glPopMatrix();
}

//this is a jupiter, because I said so.
void drawGiantSphere() {
	if(check_box_2==1){
		glColor3f(0, 1.0, 0.0);
		
		
	glPushMatrix();
	
	glTranslatef(-19, 7, -15);
	glRotatef(90, 1, 0, 0);
	glutWireSphere(moonR+3, 20, 20);
	glPopMatrix();
	}
}
//menu ids
void another_menu(int id) {
	if (id == 1)exit(0);
	else if(id==4)
	{	checkBoxF(dayOrNight);
		checkBoxF(check_box_3);
		glutSetWindow(id2);
		glutPostRedisplay();
		glutSetWindow(id1);
	}
	else if(id==3)
	{
		checkBoxF(toggle_moon_drag);
		checkBoxF(check_box_4);
		glutSetWindow(id2);
		glutPostRedisplay();
		glutSetWindow(id1);
	}
	//toggle full moon
	else if (id == 2) {
		a = 90;
		radio_button = 3;
		glutSetWindow(id2);
		glutPostRedisplay();
		glutSetWindow(id1);
	}
	//rotate the sky
	else if (id == 5) {
		if (sky_rotate == 360) sky_rotate = 0;
		sky_rotate = sky_rotate+90;
		glutSetWindow(id1);
		glutPostRedisplay();
	}
	//change the size of the moon
	else if (id == 6) {
		checkBoxF(moonScale);
		
		glutSetWindow(id1);
		glutPostRedisplay();
	}
	
}
void top_menu(int id) {
	if (id == 1) exit(0);
}
void makeMenu(void) {
	glutSetWindow(id1);
	int sub_menu;
	sub_menu = glutCreateMenu(another_menu);
	//glutCreateMenu(top_menu);
	glutAddMenuEntry("Toggle Full Moon",2);
	glutAddMenuEntry("Toggle Moon Drag", 3);
	glutAddMenuEntry("Toggle Night/Day", 4);
	glutAddMenuEntry("Rotate sky 90 degrees", 5);
	glutAddMenuEntry("Switch Moon Size", 6);
	glutCreateMenu(top_menu);
	glutAddMenuEntry("Quit", 1);
	glutAddSubMenu("buttons", sub_menu);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}



//mouse control

void mouseControl(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		//for testing purposes
		/*cout << "this x is : " << x << endl;
		cout << "this y is : " << y << endl;*/
		
		if (x >= 120 && x <= 160 && y >= 100 && y <= 125) {
			checkBoxF(check_box_1);
			//cout << check_box_1 << endl;
		}
		else if (x >= 120 && x <= 160 && y >= 130 && y <= 155) {
			//2nd button
			checkBoxF(check_box_2);
			//cout << check_box_2 << endl;
		}
		else if (x >= 120 && x <= 160 &&y >= 160 && y <= 185) {
			//3rd button
			checkBoxF(check_box_3);
			checkBoxF(dayOrNight);
			//cout << check_box_3 << endl;
		}
		else if (x >= 120 && x <= 160 && y >= 190 && y <= 215) {
			//4th button
			checkBoxF(check_box_4);
			//cout << check_box_4 << endl;
			//moon drag toggled
			checkBoxF(toggle_moon_drag);
		}

		//radio button:
		else if (x >= 245 && x <= 295 && y >= 100 && y <= 125) {
			radio_button = 0;
			
		}
		else if (x >= 245 && x <= 295 && y >= 130 && y <= 155) {
			radio_button = 1;
		}
		else if (x >= 245 && x <= 295 && y >= 160 && y <= 185) {
			radio_button = 2;
		}
		else if (x >= 245 && x <= 295 && y >= 190 && y <= 215) {
			radio_button = 3;
		}

	}

	


	
	glutPostRedisplay();
}

void mouseMotion(int x, int y) {
	int i = display_w / view_w / 2;
	int j = display_h / view_h / 2;
	//reason: the window cord starts in the middle point, which is half of the display_w
	//y is backwards, I forgot why(prob has something to do with window position)
	if(toggle_moon_drag){
	moonX = double(x-display_w/2)/i;
	moonY = double(display_h/2-y) /j;
	//cout << "y is :" << moonY << endl;
	//cout << "x is :" << moonX << endl; //how I tested out the coords
	glutPostRedisplay();
	}
}

void drawStar() {


	//constellation:
	//I stole this design(only the coordinates) from Ruizi Xu (with his permission)
	glPushMatrix();
	
	
	glTranslatef(-15, 0, 0);
	glRotated(20, 1, 0, 0);
	glTranslatef(15, 0, 0);
	if (check_box_1==1)	{
		
		//again the coordinates are from Ruizi Xu
		
		glColor3f(.9, 1, .2);
		glEnable(GL_LINE_STIPPLE);
		glLineStipple(1, 1000001);
		glPointSize(8);
		glBegin(GL_POINTS);
		//the followiong points come from Ruizi Xu 
		glVertex3f(10.0, 30.0, -1.0);
		glVertex3f(18.0, 25.0, -1.0);
		glVertex3f(24.0, 15.0, -1.0);
		glVertex3f(28.0, 5.0, -1.0);
		glVertex3f(22.0, -1.0, -1.0);
		glVertex3f(30.0, -7.0, -1.0);
		glVertex3f(34.0, -2.0, -1.0);
		glEnd();

		glBegin(GL_LINE_STRIP);
		glVertex3f(10.0, 30.0, -1.0);
		glVertex3f(18.0, 25.0, -1.0);
		glVertex3f(24.0, 15.0, -1.0);
		glVertex3f(28.0, 5.0, -1.0);
		glVertex3f(22.0, -1.0, -1.0);
		glVertex3f(30.0, -7.0, -1);
		glVertex3f(34.0, -2.0, -1);
		glEnd();
		glDisable(GL_LINE_STIPPLE);
		//the above points come from Ruizi Xu 
	}
	glPopMatrix();
}

//draw scene for display window
void drawScene_display(void) {
	// Choose window.
	glutSetWindow(id1);
	if (dayOrNight == 1)
		glClearColor(0.1, 0.1, 0.1, 0.0);

	else if (dayOrNight == 0)
		glClearColor(.9, .9, .9, 0.0);
		 //scene  goes here

	glClear( GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0, 0.0, 0.0);// gl color
	glEnable(GL_DEPTH_TEST);
	
	//rotating the entire sky would mess up the move drag control, because
	//the translate always comes before the rotation
	//unless we rotate the sky only and not the moon	
	//glRotated(sky_rotate, 0.0, 0.0, 1);
	drawMoon();
	glPushMatrix();
	glRotated(sky_rotate, 0.0, 0.0, 1);
	drawGiantSphere();
	drawStar();
	glPopMatrix();
	//always flush it
	glDisable(GL_DEPTH_TEST);
	glFlush();
}

//draw scene for control panel
void drawScene_control(void) {
	//
	double cb_x = -6;
	double cb_y = 22;
	double cb_z = 1.0;

	// Choose window.
	glutSetWindow(id2);
	glClear(GL_COLOR_BUFFER_BIT);


	glColor3f(1.0, 0.0, 0.0);// gl color

	 //scene  goes here
	glLineWidth(1);
	glColor3f(0.5, 0.5, 0.5);
	//first box
	glPushMatrix();
	glTranslatef(cb_x, cb_y, cb_z);
	glutSolidCube(4);
	glPopMatrix();
	
	
	glColor3f(0.5, 0.5, 0.5);
	//second box
	glPushMatrix();
	glTranslatef(cb_x, cb_y-5, cb_z);
	glutSolidCube(4);
	glPopMatrix();

	//third box
	glPushMatrix();
	glTranslatef(cb_x, cb_y-10, cb_z);
	glutSolidCube(4);
	glPopMatrix();
	//fourth box
	glPushMatrix();
	glTranslatef(cb_x, cb_y-15, cb_z);
	glutSolidCube(4);
	glPopMatrix();

	//crosses
	if (check_box_1 == 1) {
		glLineWidth(2);
		glBegin(GL_LINES);
		glColor3f(1.0, 1.0, 1.0);// gl color
		glVertex3f(cb_x - 1.7, cb_y - 1.7, 0.1);
		glVertex3f(cb_x + 1.7, cb_y + 1.7, 0.1);
		glVertex3f(cb_x - 1.7, cb_y + 1.7, 0.1);
		glVertex3f(cb_x + 1.7, cb_y - 1.7, 0.1);
		glEnd();
		
		
	}
	if (check_box_2 == 1) {
		glLineWidth(2);
		glBegin(GL_LINES);
		glColor3f(1.0, 1.0, 1.0);// gl color
		glVertex3f(cb_x - 1.7, cb_y - 1.7-5, 0.1);
		glVertex3f(cb_x + 1.7, cb_y + 1.7-5, 0.1);
		glVertex3f(cb_x - 1.7, cb_y + 1.7-5, 0.1);
		glVertex3f(cb_x + 1.7, cb_y - 1.7-5, 0.1);
		glEnd();
	}
	if (check_box_3 == 1) {
		glLineWidth(2);
		glBegin(GL_LINES);
		glColor3f(1.0, 1.0, 1.0);// gl color
		glVertex3f(cb_x - 1.7, cb_y - 1.7 - 10, 0.1);
		glVertex3f(cb_x + 1.7, cb_y + 1.7 - 10, 0.1);
		glVertex3f(cb_x - 1.7, cb_y + 1.7 - 10, 0.1);
		glVertex3f(cb_x + 1.7, cb_y - 1.7 - 10, 0.1);
		glEnd();
	}
	if (check_box_4 == 1) {
		glLineWidth(2);
		glBegin(GL_LINES);
		glColor3f(1.0, 1.0, 1.0);// gl color
		glVertex3f(cb_x - 1.7, cb_y - 1.7 - 15, 0.1);
		glVertex3f(cb_x + 1.7, cb_y + 1.7 - 15, 0.1);
		glVertex3f(cb_x - 1.7, cb_y + 1.7 - 15, 0.1);
		glVertex3f(cb_x + 1.7, cb_y - 1.7 - 15, 0.1);
		glEnd();
		
	}

	//radio buttons
	glLineWidth(1);
	double radioR = 2.2;
	double radioX = 7;
	double radioY = 22;
	double radioZ = 1.0;
	glPushMatrix();
	glTranslated(radioX, radioY, radioZ);
	glColor3f(.4, .5, .2);
	glBegin(GL_LINE_STRIP);
	for (double i = 0; i < 360; i++) {
		glVertex3f(radioR*cos(i), radioR*sin(i),1.0 );
	}
	glEnd();

	glBegin(GL_LINE_STRIP);
	for (double i = 0; i < 360; i++) {
		glVertex3f(radioR*cos(i), radioR*sin(i)-5.2, 1.0);
	}
	glEnd();
	

	glBegin(GL_LINE_STRIP);
	for (double i = 0; i < 360; i++) {
		glVertex3f(radioR*cos(i), radioR*sin(i) - 10.4, 1.0);
	}
	glEnd();

	glBegin(GL_LINE_STRIP);
	for (double i = 0; i < 360; i++) {
		glVertex3f(radioR*cos(i), radioR*sin(i) - 15.6, 1.0);
	}
	glEnd();

	glPopMatrix();

	//radio button show
	if (radio_button == 0) {
		glColor3f(1, 0, .2);
		glPushMatrix();
		glTranslated(radioX, radioY, radioZ-1);
		glutSolidSphere(radioR - 0.3, 20, 20);
		glPopMatrix();
		//new moon
		a = 270;
		
	}
	else if (radio_button == 1) {
		glColor3f(1, 0, .2);
		glPushMatrix();
		glTranslated(radioX, radioY - 5.2, radioZ - 1);
		glutSolidSphere(radioR - 0.3, 20, 20);
		glPopMatrix();
		//crescent moon
		a = 315;
	}
	else if (radio_button == 2) {
		glColor3f(1, 0, .2);
		glPushMatrix();
		glTranslated(radioX, radioY -10.4, radioZ - 1);
		glutSolidSphere(radioR - 0.3, 20, 20);
		glPopMatrix();
		//half moon
		a = 0;
	}
	else if (radio_button == 3) {
		glColor3f(1, 0, .2);
		glPushMatrix();
		glTranslated(radioX, radioY - 15.6, radioZ - 1);
		glutSolidSphere(radioR - 0.3, 20, 20);
		glPopMatrix();
		//full moon
		a = 90;
	}

	//instructions for buttons
	//radio button instruction
	glColor3f(0.0, 0.0, 1.0);
	glRasterPos3f(radioX + 4, radioY, radioZ);
	writeBitmapString((void*)font, "new moon");

	glColor3f(0.0, 0.0, 1.0);
	glRasterPos3f(radioX +4, radioY-5, radioZ);
	writeBitmapString((void*)font, "crescent");

	glColor3f(0.0, 0.0, 1.0);
	glRasterPos3f(radioX + 4, radioY-10, radioZ);
	writeBitmapString((void*)font, "half moon");

	glColor3f(0.0, 0.0, 1.0);
	glRasterPos3f(radioX + 4, radioY-15, radioZ);
	writeBitmapString((void*)font, "full moon");

	//check box instructions
	glRasterPos3f(radioX-23, radioY, radioZ);
	writeBitmapString((void*)font, "Toggle Constellation");

	glRasterPos3f(radioX - 23, radioY-5, radioZ);
	writeBitmapString((void*)font, "That Jupiter");

	glRasterPos3f(radioX - 23, radioY - 10, radioZ);
	writeBitmapString((void*)font, "Day/Night");

	glRasterPos3f(radioX - 23, radioY - 15, radioZ);
	writeBitmapString((void*)font, "Drag Moon");


	//go back to display window, draw it, and come back
	glutSetWindow(id1);
	glutPostRedisplay();
	glutSetWindow(id2);

	//always flush it
	glFlush();
}



//// Drawing routine.
//void drawScene(void)
//{
//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//
//	glMatrixMode(GL_MODELVIEW);
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear the buffers including
//														// the depth buffer.
//  glLoadIdentity();
//  glEnable(GL_DEPTH_TEST); // Enable depth testing.
// 
//
//  glDisable(GL_DEPTH_TEST); // Disable depth testing.
//
//							// Write labels.
//  glColor3f(1.0, 0.0, 0.0);
// 
//  glutSwapBuffers();
//}
//
//// Initialization routine.
//void setup(void)
//{
//	glEnableClientState(GL_VERTEX_ARRAY);
//  //blue background
//	glClearColor(0, .7, 1, 0.0);
//}

// initialization for both windows
void setup1(void) {
	if (dayOrNight == 1)
		glClearColor(0.1, 0.1, 0.1, 0.0);
	
	else if( dayOrNight ==0)
		glClearColor(.9, .9, .9,0.0);
	
}

void setup2(void) {
	glClearColor(0.0, 1.0, 0.0, 0.0);
}

// OpenGL window reshape routine.
void resize1(int w, int h)
{
  glViewport(0, 0, (GLsizei)w, (GLsizei)h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
    //view box
	glOrtho(-view_w, view_w, -view_h, view_h, -view_d, view_d);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}
	//resize 2 for control
void resize2(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-view_w/2, view_w/2, -view_h,view_h, -view_d, view_d);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
  switch(key)
  {
    case 27:
      exit(0);
      break;
	case 'c':
		//angle wont go over 360.
		if(a<=360) a = a+1;
		else  a = 0;
		glutSetWindow(id1);
		glutPostRedisplay();
		//cout << "angle is : " << a<<endl;
		break;
	case 'd':
		//toggles day or night
		checkBoxF(dayOrNight); 
		checkBoxF(check_box_3);
		glutSetWindow(id1);
		glutPostRedisplay();
		glutSetWindow(id2);
		glutPostRedisplay();
		break;
	case 's':
		//toggle stars
		checkBoxF(check_box_1);
		glutSetWindow(id1);
		glutPostRedisplay();
		glutSetWindow(id2);
		glutPostRedisplay();
		break;
	case'j' :
		//toggle jupiter
		checkBoxF(check_box_2);
		glutSetWindow(id1);
		glutPostRedisplay();
		glutSetWindow(id2);
		glutPostRedisplay();
		break;
	case'm':
		//toggle moon drag
		checkBoxF(check_box_4);
		checkBoxF(toggle_moon_drag);
		glutSetWindow(id1);
		glutPostRedisplay();
		glutSetWindow(id2);
		glutPostRedisplay();
		break;
	case'p':
		//iterate from new moon to full moon
		if (radio_button >= 3)radio_button = 0;
		else radio_button = radio_button + 1;
		glutSetWindow(id1);
		glutPostRedisplay();
		glutSetWindow(id2);
		glutPostRedisplay();
		break;
	default:
      break;
  }
}

// Routine to output interaction instructions to the C++ window.
void printInteraction(void)
{
  cout << "Interaction:" << endl;
  cout << "press 'm' to toggle moon drag"<<endl;
  cout << "press 'j' to show Jupiter" << endl;
  cout << "press 's' to show big dipper" << endl;
  cout << "press 'd' toggle between day and night" << endl;
  cout<<"press 'p'to iterate from new moon to full moon"<<endl;
  cout << "press esc to quit" << endl;
  cout << "once toggled moon drag, you can click and drag moon to where ever you want." << endl;
}

// Main routine.
int main(int argc, char **argv)
{
  printInteraction();

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

  //first window
  glutInitWindowSize(display_w, display_h);
  glutInitWindowPosition(display_position_x, display_position_y);

  //create the first window and return id
  id1=glutCreateWindow("night sky-display");
  setup1();
  
  //initialization, display and other routines.
  glutDisplayFunc(drawScene_display);
  glutReshapeFunc(resize1);
  glutKeyboardFunc(keyInput);
  
  glutMotionFunc(mouseMotion);

  // Second top-level window definition.
  glutInitWindowSize(control_w, display_h);
  glutInitWindowPosition(display_position_x+display_w, display_position_y);

  // Create the second window and return id.
  id2 = glutCreateWindow("night sky-control");

  // Initialization, display, and other routines of the second window. 
  setup2();
  glutDisplayFunc(drawScene_control);
  glutMouseFunc(mouseControl);
  glutReshapeFunc(resize2);
  glutKeyboardFunc(keyInput); // Routine is shared by both windows.
  makeMenu();
  glutMainLoop();

  return 0;
}
