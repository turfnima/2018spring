/////////////////////////////////          
// boxV3.cpp
//
// This program draws a wire teapot.
// m: reflected
//
// Sumanta Guha/Marjory Baruch
/////////////////////////////////

#include <iostream>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

using namespace std;
static int isModified=0;

// Drawing routine.
void drawScene(void)
{
   glClear(GL_COLOR_BUFFER_BIT);
   glColor3f(0.0, 0.0, 0.0);
   glLoadIdentity(); 
   
   // Modeling transformations.
   glTranslatef(0.0, 0.0, -15.0);
  
  if (!isModified)
  {
    glScalef(1.0,1.0,1.0);
    glColor3f(1.0,0.0,0.0);
    glutWireTeapot(3.0);
  }
   
  if (isModified)
  {
    glScalef(1.0,-1.0,1.0);
    glColor3f(0.0,0.0,0.0);
    glutWireTeapot(3.0);
  }
  
   glFlush();
}

// Initialization routine.
void setup(void) 
{
   glClearColor(1.0, 1.0, 1.0, 0.0);  
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
   glViewport(0, 0, (GLsizei)w, (GLsizei)h); 
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glFrustum(-5.0, 5.0, -5.0, 5.0, 5.0, 100.0);

   glMatrixMode(GL_MODELVIEW);
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
   switch(key) 
   {
      case 27:
         exit(0);
         break;
     case 'm':
       isModified=!isModified;
       glutPostRedisplay();
       break;
      default:
         break;
   }
}

// Routine to output interaction instructions to the C++ window.
void printInteraction(void)
{
  cout << "Interaction:" << endl;
  cout << "Press m to reflect across the xz-plane." << endl;
  
}

// Main routine.
int main(int argc, char **argv) 
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); 
   glutInitWindowSize(500, 500);
   glutInitWindowPosition(100, 100); 
   glutCreateWindow("boxV3.cpp");
   setup(); 
   glutDisplayFunc(drawScene); 
   glutReshapeFunc(resize);  
   glutKeyboardFunc(keyInput);
   printInteraction();
   glutMainLoop(); 

   return 0;  
}

