/////////////////////////////////
// pushPopDemo.cpp
//
// Draws a rotated box and places it
// Independently draws a cone,
//     stands it up and places it.
//
// Marjory Baruch
/////////////////////////////////

#include <iostream>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

using namespace std;

// Drawing routine.
void drawScene(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0, 0.0, 0.0);
    glLoadIdentity();
    
    // a rotated cube centered at 10, 0, -15
    glPushMatrix();
    glTranslatef(10.0, 0.0, -15.0);
    glRotatef(30, 0, 0, 1);
    glutWireCube(2.0); // Box.
    glPopMatrix();
    
    // a rotated cone centered at -5, 0, -10
    glPushMatrix();
    glTranslatef(-5.0, 0.0, -10.0);
    glRotatef(90, -1, 0, 0);
    glutWireCone(2, 5, 10, 10); // cone
    glPopMatrix();
    
    glFlush();
}

// Initialization routine.
void setup(void)
{
    glClearColor(1.0, 1.0, 1.0, 0.0);
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //glFrustum(-5.0, 5.0, -5.0, 5.0, 5.0, 100.0);
    glOrtho(-15, 15, -15, 15, 0, 20);
    glMatrixMode(GL_MODELVIEW);
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
    switch(key)
    {
        case 27:
            exit(0);
            break;
        default:
            break;
    }
}

// Main routine.
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("pushPopDemo.cpp");
    setup();
    glutDisplayFunc(drawScene);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyInput);
    glutMainLoop();
    
    return 0;  
}

