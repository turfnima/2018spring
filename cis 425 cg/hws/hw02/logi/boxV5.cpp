/////////////////////////////////          
// boxV5.cpp
//
// This program draws a wire box.
// t: code says translate, rotate
// r: code says rotate, translate
//
// Sumanta Guha/Marjory Baruch
/////////////////////////////////

#include <iostream>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

using namespace std;
static int codeSaysTransRot=0, codeSaysRotTrans=0;

// Drawing routine.
void drawScene(void)
{
   glClear(GL_COLOR_BUFFER_BIT);
   glColor3f(0.0, 0.0, 0.0);
   glLoadIdentity(); 
   
   // Modeling transformations.
   glTranslatef(0.0, 0.0, -15.0);
  
  if (!codeSaysTransRot & !codeSaysRotTrans)
  {
    glColor3f(1.0,0.0,0.0);
    glutWireCube(5.0);
  }
   
  if (codeSaysTransRot)
  { 
    glColor3f(0.0,0.0,0.0);
    glTranslatef(10.0,0.0,0.0);
    glRotatef(45.0, 0.0,0.0,1.0);
    glutWireCube(5.0);
  }
  
  if (codeSaysRotTrans)
  {
    glColor3f(0.0,0.0,0.0);
    glRotatef(45.0, 0.0,0.0,1.0);
    glTranslatef(10.0,0.0,0.0);
    glutWireCube(5.0);
  }
  
  
   glFlush();
}

// Initialization routine.
void setup(void) 
{
   glClearColor(1.0, 1.0, 1.0, 0.0);  
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
   glViewport(0, 0, (GLsizei)w, (GLsizei)h); 
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glFrustum(-5.0, 5.0, -5.0, 5.0, 5.0, 100.0);
   //glOrtho(-20,20,-20,20,5,100);

   glMatrixMode(GL_MODELVIEW);
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
   switch(key) 
   {
      case 27:
         exit(0);
         break;
     case 't':
       codeSaysTransRot=!codeSaysTransRot;
       codeSaysRotTrans=0;
       glutPostRedisplay();
       break;
     case 'r':
       codeSaysRotTrans=!codeSaysRotTrans;
       codeSaysTransRot=0;
       glutPostRedisplay();
      default:
         break;
   }
}

// Routine to output interaction instructions to the C++ window.
void printInteraction(void)
{
  cout << "Interaction:" << endl;
  cout << "Press t for Code says translate, rotate." << endl;
  cout << "Press r for Code says rotate, translate." << endl;
  
}

// Main routine.
int main(int argc, char **argv) 
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); 
   glutInitWindowSize(500, 500);
   glutInitWindowPosition(100, 100); 
   glutCreateWindow("boxV5.cpp");
   setup(); 
   glutDisplayFunc(drawScene); 
   glutReshapeFunc(resize);  
   glutKeyboardFunc(keyInput);
   printInteraction();
   glutMainLoop(); 

   return 0;  
}

