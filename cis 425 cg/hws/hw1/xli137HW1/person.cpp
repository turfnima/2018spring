/*interactioin guideline:
Interaction:
// Press V/v to increase/decrease the number of longitudinal (verticle) slices.
// Press H/h to increase/decrease the number of latitudinal (horizontal) slices.
// Press p toggle between "ortho" and "frustum" views

*/





///////////////////////////////////////////////////////////////////////////////////////
// person.cpp
// Author:  Baruch
//
// This program draws a person. -> draws 3 person, a helix, and some cylinder
//
// Interaction:
// Press V/v to increase/decrease the number of longitudinal (verticle) slices.
// Press H/h to increase/decrease the number of latitudinal (horizontal) slices.
//
//
//  modified by : Xiaozhi Li
//
/*modifications:

those minimum requied by hw:
1 check	Use a variety of 2D primitives, including a triangle strip and a triangle fan.
2 check		Use both filled and outlined modes.
3 check Use a variety of colors.
4 check	     Display some text.
5 check		 Use a vertex array and a color array with glVertexPointer.
6 check		 More than one person. At least one at a different z-value.
7 check		 A cylinder for a body. Build the cylinder from 2D primitives. (Not gluCylinder.)
8 check     Both parallel (glOrtho) and perspective (glFrustum) views, changeable with some keyboard choice. Toggle back and forth using the key P.
9 check		Instructions in 3 places explaining keyboard choices:
			in the console window
			a comment at the top of the program
			in the brag sheet

other modifications:
1	bented arm, circular arm
2	cone, I put a hat on one of the person.
3   multiple person
4   there is a toggle view instruction on the draw window, and it does not use a if statement
5   changing color (every time the scene changes, there will be random color assigned)
6	some helix (rainbow)
*/
///////////////////////////////////////////////////////////////////////////////////////

//normal logistic
#include <cmath>
#include <iostream>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

#define PI 3.14159265

using namespace std;
//end of normal logistic

// Globals.
static float R = 1.0; // Radius of head.
static int v = 6; // Number of longitudinal (verticle) slices


//console text
static long font = (long)GLUT_BITMAP_8_BY_13; // Font selection.
// Routine to draw a bitmap character string.
void writeBitmapString(void *font, char *string)
{
	char *c;

	for (c = string; *c != '\0'; c++) glutBitmapCharacter(font, *c);
}

static int h = 4; // Number of latitudinal (horizontal) slices
//on hemisphere.
static float cx=2.0, cy=3.0, cz=-11.0;  //center of sphere
static float bodylength=4.0;
static float armlength=4.0;
static float leglength=5.0;
static float armangle=-PI/3.0;
static int   turns = 20;
//a negative angle goes the other way
static float legangle=-(4.0*PI/7.0);
//static float legangle=-(1.0*PI/10.0);

//the choice of perspective, swithcing from frustum and ortho
int perspectiveChoice = 0;

//some helper for the spiral helix
static float hsX = -25;
static float hsY = 5;
static float hsZ = -20;
static float t = -10;
static float hsR = 2;
static float helixV[1998];
static float helixColor[1998];

//draw cylinder
static float clynderX = -15;
static float clynderY = 5;
static float clynderZ = -28;
static float clynderR = 2;
static float clynderH = 12;
static int clynderPoint = 15;
void drawCylinder() {
	glLineWidth(1);

	//switching between gl_line and gl_fill, by perspective 'p'
	if(perspectiveChoice==0){
		clynderX = -15;
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{ 
		//gl fill
		clynderX = 23;
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_TRIANGLE_STRIP);
	for (int j = 0; j <= clynderPoint; j++)
	{
		glVertex3f(clynderX+(clynderR)*cos((float)(j) * 2*PI / clynderPoint) , clynderY,
			-(clynderR )*sin((float)(j)* 2 * PI / clynderPoint) + clynderZ);
		glVertex3f(clynderX + (clynderR)*cos((float)(j+1)* 2 * PI / clynderPoint), clynderY-clynderH,
			-(clynderR)*sin((float)(j+1)* 2 * PI / clynderPoint) + clynderZ);
		//cout << "x= " << clynderX + (clynderR)*cos((float)(j)* PI / clynderPoint) << endl;
		//cout << "Y= " << clynderY << endl;
		//cout << "z= " << -(clynderR)*sin((float)(j)* PI / clynderPoint) + clynderZ<< endl;
	}
	glEnd();
}

//a triangle strip hat
void drawHat() {
	//change back the line width
	glLineWidth(1);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glColor3f((float)rand() / (float)RAND_MAX, 0.0, (float)rand() / (float)RAND_MAX);
	// triangle fan test -hw check cone
	// -hw check triangle fan
	glBegin(GL_TRIANGLE_FAN);
	glVertex3f(cx, cy + 2*R + 1, cz); //first draw the top point of the hat
	//on my computer some time the hat does not show propperly
	for (int j = 0; j <= 15; j++) //then loop the cone by points in a circle
	{
		glVertex3f((R + 1)*cos((float)(j) / PI / 2.0) + cx, cy,
			(R + 1)*sin((float)(j) / PI / 2.0) + cz);
	}
	glEnd();
}
//a spiral helix
//made with vertex array
void drawHelix() {
	//glColor3f(0.0, 1.0, 0.0);
	//glBegin(GL_LINE_STRIP);
	/*for (t = -10; t <= 10; t += 1 / 200.0) {
	glVertex3f(hsX+PI*t, hsY+hsR*sin(t*PI), hsZ+hsR*cos(t*PI));
	}*/

	//glEnd();
	//put this in draw helix so it is not too far
	//away
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	t = -10.0;

	//assign helix color points to the array
	for (int i = 0; i < 1998; i += 3) {
		helixColor[i] = (float)rand() / (float)RAND_MAX;
		helixColor[i + 1] = (float)rand() / (float)RAND_MAX;
		helixColor[i + 2] = (float)rand() / (float)RAND_MAX;
	}
	//assign helix vertex points to the array
	for (int k = 0; k < 1998; k += 3) {
		helixV[k] = hsX - PI*t;
		//cout << "k = " << k << endl;
		//cout << "t = " << t << endl;
		helixV[k + 1] = hsY + hsR*sin(1.5 * t*PI);
		helixV[k + 2] = hsZ + hsR*cos(1.5 * t*PI);
		t += 1 / 200.0;
	}

	//(size, type, stride, *pointer);
	//unfinished!!!
	glVertexPointer(3, GL_FLOAT, 0, helixV);
	glColorPointer(3, GL_FLOAT, 0, helixColor);

	//change the line width for better view
	glLineWidth(5);
	glBegin(GL_LINE_STRIP);
	//cout << "gl line strip" << endl;
	for (int i = 0; i<666; i++) {
		glArrayElement(i);
	}
	//cout << "for loop glarray element" << endl;



	glEnd();
}

void drawHead()
{
  int i, j;
  // Based on hemisphere.cpp
  // cx, cy,cz is the center of the sphere, R is the radius.
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glColor3f(0.0, 0.0, 0.0);

  for(j = 0; j < h; j++)
  {
    // One latitudinal triangle strip. top half
    glBegin(GL_TRIANGLE_STRIP);
    for(i = 0; i <= v; i++)
    {
      glVertex3f( R * cos( (float)(j+1)/h * PI/2.0 ) * cos( 2.0 * (float)i/v * PI )+cx,
                 R * sin( (float)(j+1)/h * PI/2.0 )+cy,
                 R * cos( (float)(j+1)/h * PI/2.0 ) * sin( 2.0 * (float)i/v * PI )+cz );
      glVertex3f( R * cos( (float)j/h * PI/2.0 ) * cos( 2.0 * (float)i/v * PI  )+cx,
                 R * sin( (float)j/h * PI/2.0  )+cy,
                 R * cos( (float)j/h * PI/2.0 ) * sin( 2.0 * (float)i/v * PI)+cz );
    }
    glEnd();
    // One latitudinal triangle strip. bottom half
    glBegin(GL_TRIANGLE_STRIP);
    for(i = 0; i <= v; i++)
    {
      glVertex3f( R * cos( (float)(j+1)/h * PI/2.0 ) * cos( 2.0 * (float)i/v * PI )+cx,
                 -1*R * sin( (float)(j+1)/h * PI/2.0 )+cy,
                 R * cos( (float)(j+1)/h * PI/2.0 ) * sin( 2.0 * (float)i/v * PI )+cz );
      glVertex3f( R * cos( (float)j/h * PI/2.0 ) * cos( 2.0 * (float)i/v * PI  )+cx,
                 -1*R * sin( (float)j/h * PI/2.0  )+cy,
                 R * cos( (float)j/h * PI/2.0 ) * sin( 2.0 * (float)i/v * PI)+cz );
    }
    glEnd();
  }
}

void drawStickBody()
{

  glLineWidth(5.0);
  glColor3f((float)rand() / (float)RAND_MAX, (float)rand() / (float)RAND_MAX, (float)rand() / (float)RAND_MAX);
  glBegin(GL_LINES);
  glVertex3f(cx,cy-R,cz);
  glVertex3f(cx,cy-R-bodylength,cz);
  glEnd();
  glLineWidth(1.0);
  glColor3f(0.0,0.0,0.0);

}

void drawStickArms()
{
  glLineWidth(5.0);
  glColor3f(1.0, (float)rand() / (float)RAND_MAX, (float)rand() / (float)RAND_MAX);
  glBegin(GL_LINES);
  glVertex3f(cx,cy-R-.2*bodylength,cz); //down 20% of body
  glVertex3f(cx - 1, cy - R - .2*bodylength - 2, cz);

  glVertex3f(cx - 1, cy - R - .2*bodylength - 2, cz);
  glVertex3f(cx+armlength*cos(armangle),
             cy-R-.2*bodylength+armlength*sin(armangle),cz);


  glEnd();
  glBegin(GL_LINES);
  glVertex3f(cx,cy-R-.2*bodylength,cz); //down 20% of body
  glVertex3f(cx+1.8,
             cy-R-.2*bodylength,cz);

  glVertex3f(cx + 1.8,
	  cy - R - .2*bodylength, cz);
  glVertex3f(cx + 2.4,
	  cy - R - .2*bodylength+1.8, cz);
  glEnd();
  glLineWidth(1.0);
  glColor3f(0.0,0.0,0.0);
}



void drawStickLegs()
{
  glLineWidth(5.0);
  glColor3f((float)rand() / (float)RAND_MAX, (float)rand() / (float)RAND_MAX, (float)rand() / (float)RAND_MAX);
  glBegin(GL_LINES);
  glVertex3f(cx,cy-R-bodylength,cz); //down at bottom of body
  glVertex3f(cx+leglength*cos(legangle),
             //cy-R-.2*bodylength+leglength*sin(legangle),cz);
    cy-R-bodylength+leglength*sin(legangle),cz);//
  glVertex3f(cx,cy-R-bodylength,cz); //down at bottom of body
  glVertex3f(cx-leglength*cos(legangle),
             //cy-R-.2*bodylength+leglength*sin(legangle),cz);
   cy-R-bodylength+leglength*sin(legangle),cz);//
  glEnd();
  glLineWidth(1.0);
  glColor3f(0.0,0.0,0.0);
}

// modified draw

void drawStickArms2()
{
	//length=armlength/turns, angle=armangle/turns
	float length = armlength / turns;
	float angle = armangle / turns;

	//this should give us a little angle
	//then we for loop it

	glLineWidth(5.0);
	glColor3f((float)rand() / (float)RAND_MAX, (float)rand() / (float)RAND_MAX, (float)rand() / (float)RAND_MAX);
	glBegin(GL_LINE_STRIP);

	// -hw check poses
	// i made this person turn arm like a circle, I should have been using parabola, but I didn't figure it out.
	//glVertex3f(cx, cy - R - .2*bodylength, cz); //down 20% of body
	for(int i=turns;i>0;i--){
		glVertex3f(cx + armlength *cos(float(i)/turns*PI/2.0), cy - R - 1.2*bodylength + armlength * sin(float(i) / turns*PI / 2.0), cz);

	}
	glEnd();
	glBegin(GL_LINE_STRIP);
	//glVertex3f(cx, cy - R - .2*bodylength, cz); //down 20% of body
	for (int i = turns; i>0; i--) {
		glVertex3f(cx - armlength *cos(float(i ) / turns*PI / 2.0), cy - R - 1.2*bodylength + armlength * sin(float(i ) / turns*PI / 2.0), cz);

	}
	//glVertex3f(cx - armlength*cos(armangle),
		//cy - R - .2*bodylength + armlength*sin(armangle), cz);
	glEnd();
	glLineWidth(1.0);
	glColor3f(0.0, 0.0, 0.0);
}

// Drawing routine.
void drawScene(void)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (perspectiveChoice == 0) {
		glOrtho(-20.0, 20.0, -20.0, 20.0, 0.0, 40.0);
	}
	//frustum view
	else
	{
		glFrustum(-20.0, 20.0, -20.0, 20.0, 10.0, 40.0);
	}
	glMatrixMode(GL_MODELVIEW);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear the buffers including
														// the depth buffer.
  glLoadIdentity();
  glEnable(GL_DEPTH_TEST); // Enable depth testing.
  drawHelix();
  drawCylinder();
  cx = 2; cy = 3; cz = -15;
  legangle = -(4.0*PI / 7.0);
  drawHat();
  drawHead();
  drawStickBody();
  drawStickArms();
  drawStickLegs();

  //can we pass parameters into draw heads?
  cx = 8.5; cy = 4; cz = -17;
  legangle = -(4.0*PI / 12.0);
  drawHead();
  drawStickBody();
  drawStickArms2();
  drawStickLegs();

  cx = -4; cy = 2; cz = -25;
  legangle = -(4.0*PI / 5.0);
  drawHead();
  drawStickBody();
  drawStickArms();
  drawStickLegs();
  glDisable(GL_DEPTH_TEST); // Disable depth testing.

							// Write labels.
  glColor3f(1.0, 0.0, 0.0);
  glRasterPos3f(-18.0, -15.0, 0.0);
  writeBitmapString((void*)font, "Ortho view, Press 'p' to toggle frustum view");
  glRasterPos3f(-35.0, 12, -20.0);
  writeBitmapString((void*)font, "Frustum view, press 'p' to toggle ortho view");
  glutSwapBuffers();
}

// Initialization routine.
void setup(void)
{
	glEnableClientState(GL_VERTEX_ARRAY);
  //blue background
	glClearColor(0, .7, 1, 0.0);
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
  glViewport(0, 0, (GLsizei)w, (GLsizei)h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

	glOrtho(-20.0, 20.0, -20.0, 20.0, 0.0, 40.0);

  glMatrixMode(GL_MODELVIEW);
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
  switch(key)
  {
    case 27:
      exit(0);
      break;
    case 'V':
      v += 1;
      glutPostRedisplay();
      break;
    case 'v':
      if (v > 3) v -= 1;
      glutPostRedisplay();
      break;
    case 'H':
      h += 1;
      glutPostRedisplay();
      break;
    case 'h':
      if (h > 3) h -= 1;
      glutPostRedisplay();
      break;
	case 'p':
		if (perspectiveChoice == 0)perspectiveChoice = 1;
		else perspectiveChoice = 0;
		glutPostRedisplay();
		break;
    default:
      break;
  }
}

// Routine to output interaction instructions to the C++ window.
void printInteraction(void)
{
  cout << "Interaction:" << endl;
  cout << "Press V/v to increase/decrease the number of longitudinal slices." << endl
  << "Press H/h to increase/decrease the number of latitudinal slices." << endl;
  cout << "Press p to switch the view between frustum and ortho" << endl;
}

// Main routine.
int main(int argc, char **argv)
{
  printInteraction();
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(500, 500);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("A Person");
  setup();
  glutDisplayFunc(drawScene);
  glutReshapeFunc(resize);
  glutKeyboardFunc(keyInput);
  glutMainLoop();

  return 0;
}
