/******************************************
*
* Official Name:  Xiaozhi LI
*
* Nickname:  Caesar
*
* E-mail:  xli137@syr.edu
*
* Assignment:  Assignment 04
*
* Environment/Compiler:  Visual Studio 2015
*
* Date:  April 2, 2018
*
* References:
*	none
interactions:
cout << "Interaction:" << endl;
cout << "l to turn on/off head light" << endl;
cout << "2 to turn on/off the natural light" << endl;
cout << "\tup arrow - step forward" << endl;
cout << "\tdown arrow - step backward" << endl;
cout << "\tright arrow - turn to the right" << endl;
cout << "\tleft arrow - turn to the left" << endl;
cout << "\tpage down - shift in positive x axis" << endl;
cout << "\tend - shift in negative x axis" << endl;
cout << "\t if you see a key, click the window, and then click drag, you will open the door" << endl;
cout << "\tr - reset to standing in the center ";
cout << "facing 0 degrees." << endl;
*******************************************/

//normal logistic
#include <cmath>
#include <iostream>
//vector for vector
#include <cstdlib>
#include <vector>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif


# include "painter.h"
#define PI 3.14159265
using namespace std;

//I don't use these by you can
static float playerX = 0, playerY = 0.0, playerZ = 0;
static float angle = 0;
static float stepsize = 0.5;  //step size
static float turnsize = 5.0; //degrees to turn

//for testing objects:
static float testAngle=0, testAngle2=0,testX = 1, testY = 1, testZ = 1;
static float testStep = 0.1;
//for moving obj around
static float objX = 0, objY = 0, objZ = 0;
static int keyMode = 0;

//for light
static float spotExponent = 10.0; // Spotlight exponent = attenuation.
static float spotAngle = 50.0; // Spotlight cone half-angle.
static bool flash = false, moonLight=true;

//for key animation
static float key_y = 0, key_x=-6, key_z=10.25;
static float key_r_x = 0, key_r_y=0, key_r_z=0;
static double someT = 0;
static bool key_appear = false;
static bool key_anim = false;
static bool key_idle = true;
static int animationPeriod = 20;
static bool door_anim = false;
static bool keylock1 = false, keylock2 = false, keylock3 = false, keylock4 = false, keylock5 = false;
static double doorAngle = 0;

static int mouseX = 0, mouseY = 0;

//from BallAndTorusPicking
int closestName;
static unsigned int buffer[1024]; // Hit buffer.
static int hits; // Number of entries in hit buffer.

bool isSelecting = false;
int isDragging = 0;

//prototypes
void levitating(void);

//console text
static long font = (long)GLUT_BITMAP_8_BY_13; // Font selection.
											  // Routine to draw a bitmap character string.
void writeBitmapString(void *font, char *string)
{
	char *c;
	for (c = string; *c != '\0'; c++) glutBitmapCharacter(font, *c);
}
// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(100, 1, 0.9, 100);
	glMatrixMode(GL_MODELVIEW);
}

//door animation
//x=-6.5
//y=-0.7
//z=10.8

void adjustLocation() {
	double hitRange = 0.1;
	double rhitRange = 1;
	//first we calculate their difference
	double xoff = key_x - (-6.5);
	double zoff = key_z - 10.8;
	double yoff = key_y - (-0.7);
	double xRoff = key_r_x +100 ;
	double zRoff = key_r_z + 100;
	//if this difference is bigger than hit range, we care, and we adjust it
	
	if (fabs(xoff) > hitRange)
	{
		if (xoff > 0.00)
		
			key_x -= 0.05;
		else
			key_x += 0.05;
		
	}
	else keylock1 = true;

	if (fabs(yoff) > hitRange)
	{
		if (yoff > 0.00)

			key_y -= 0.05;
		else
			key_y += 0.05;
	}
	else keylock2 = true;

	if (fabs(zoff) > hitRange)
	{
		if (zoff > 0.00)

			key_z -= 0.05;
		else
			key_z += 0.05;
	}
	else keylock3 = true;

	if (fabs(xRoff) > rhitRange)
	{
		if (xRoff > 0.00)
			key_r_x -= 5;
		else
			key_r_x += 1;
	}
	else keylock4 = true;
	if (fabs(zRoff) > rhitRange)
	{
		if (zRoff > 0.00)
			key_r_z -= 5*5;
		else
			key_r_z += 5*5;
	}
	else keylock5 = true;
	
	if (keylock2&&keylock3&&keylock4&&keylock5&&keylock1) {
		door_anim = false;
		key_anim = true;
	}
}
void door_open() {
	adjustLocation();
}

//door animate

void animate(int someValue) {
	if (door_anim)door_open();

	glutTimerFunc(animationPeriod, animate, 1);
	glutPostRedisplay();
}



//for testing objects
void drawTest(void)
{
	Painter a;
	//draw floor
	//a.drawFloor();
	//drawhouse
	glEnable(GL_DEPTH_TEST);
	a.drawFloor();

	//house
	glPushMatrix();
	glTranslatef(-3, .6, 11);
	//glRotatef(20, 0, 1, 0);
	a.drawHouse();

	glPushMatrix();
	glTranslatef(-2.4, -1.3, 0);
	glRotatef(doorAngle, 0, 1, 0);
	//draw goes here
	a.drawDoor();
	//end draw
	glPopMatrix();

	//end draw
	glPopMatrix();

	//draw moon
	glPushMatrix();
	glTranslatef(-98, 75, 53);
	//draw goes here
	a.drawMoon(30);
	//end draw
	glPopMatrix();


	//draw key
	//only when key is triggered
	if (fabs((playerX+6)<4) && fabs(playerZ-10<4)) {
		key_appear = true;
		glPushMatrix();
		
		
		//draw goes here
		glTranslatef(key_x, key_y, key_z);
		glRotatef(key_r_y, 0, 1, 0);
		glRotatef(key_r_z, 0, 0, 1);
		glRotatef(key_r_x, 1, 0, 0);
		glScalef(0.25, 0.25, 0.25);
		a.drawKey();
		//end draw
		glPopMatrix();
	}
	if (key_idle) {
		glutIdleFunc(levitating);
	}
	else {
		glutIdleFunc(NULL);
	}

	//draw anything use this
	glPushMatrix();
	glTranslatef(-3, 0, 11);
	glRotatef(0, 0, 0, 0);
	//draw goes here

	//end draw
	glPopMatrix();
	//draw flowers
	glPushMatrix();
	glTranslatef(4, -1.5, 15);
	glRotatef(0, 0, 0, 0);
	//draw goes here
	for (double i = 0; i<8; i++) {
		for (double j = 0; j<5; j++) {
			a.drawFlower(0.25*i, 0, 0.25*j);
		}
	}
	//end draw
	glPopMatrix();

	//draw a box
	glPushMatrix();
	glTranslatef(-3, -1.6, 15);
	//draw goes here
	a.drawBox();
	//end draw
	glPopMatrix();

	glPushMatrix();

	glTranslatef(objX, objY, objZ);
	//glTranslatef(0, 1, 2.5);
	
	glRotatef(testAngle2, 1, 0, 0);
	glRotatef(testAngle, 0, 1, 0);
	glScalef(testX, testX, testX);
	//test object goes here
	//a.drawWall(2, 2, 0.5);
	glScalef(0.25, 0.25, 0.25);

	
	//end test object

	glPopMatrix();
	glDisable(GL_DEPTH_TEST);
}

// Initialization routine.
void setup(void)
{
	//glClearColor(1.0, 1.0, 1.0, 0.0);
	//glEnable(GL_DEPTH_TEST); // Enable depth testing.

	glClearColor(0.0, 0.0, 0.0, 0.0);

	// Turn on OpenGL lighting.
	glEnable(GL_LIGHTING);

	// Light property vectors.
	float lightAmb[] = { 0.1, 0.1, 0.1, 1.0 };
	float lightDif[] = { 0.8, 0.8, .8, 1.0 };
	float lightSpec[] = { 1,1,1, 1.0 };
	float globAmb[] = { 0.1, 0.1, 0.1, 1.0 };


	float lightAmb2[] = { 0.0, 0.0, 0.0, 1.0 };
	float lightDif2[] = { 0.6, 0.6, 0.6, 1.0 };
	float lightSpec2[] = { 0.5,0.5,0.5, 1.0 };
	float lightPos2[] = { 1.0, 3.0, 14, 1.0 };
	// HeadLight properties.
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmb);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDif);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpec);
	// ceiling light properties
	glLightfv(GL_LIGHT1, GL_AMBIENT, lightAmb2);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightDif2);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lightSpec2);
	glLightfv(GL_LIGHT1, GL_POSITION, lightPos2);
	glEnable(GL_LIGHT1);

	glEnable(GL_LIGHT0); // Enable particular light source.
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globAmb); // Global ambient light.
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE); // Enable local viewpoint.
	

	//not yet using these
	// Material property vectors.
	//float matSpec[] = { 0.5,0.5,0.5,1 };
	//float matShine[] = { 50.0 };
	//// Material properties shared by all the spheres.
	//glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpec);
	//glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, matShine);
	// Enable color material mode:
	// The ambient and diffuse color of the front faces will track the color set by glColor().
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
}

void printInteraction(void)
{
	cout << "Interaction:" << endl;
	cout << "l to turn on/off head light" << endl;
	cout << "2 to turn on/off the natural light" << endl;
	cout << "\tup arrow - step forward" << endl;
	cout << "\tdown arrow - step backward" << endl;
	cout << "\tright arrow - turn to the right" << endl;
	cout << "\tleft arrow - turn to the left" << endl;
	cout << "\tpage down - shift in positive x axis" << endl;
	cout << "\tend - shift in negative x axis" << endl;
	cout << "\t if you see a key, click the window, and then click drag, you will open the door" << endl;
	cout << "\tr - reset to standing in the center ";
	cout << "facing 0 degrees." << endl;
}


// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	case 'r':
		playerX = 0;
		playerZ = 0;
		angle = 0;
		glutPostRedisplay();
		break;

	case 'a':
		if (keyMode == 0) {
			if (testAngle >= 360)testAngle = 0;
			else
				testAngle = testAngle + 10;
		}
		else if (keyMode == 1) {
			objX = objX + testStep;
		}
		glutPostRedisplay();
		break;
	case 'd':
		if (keyMode == 0) {
			if (testAngle <= 0)testAngle = 360;
			else
				testAngle = testAngle - 10;
		}
		else if (keyMode == 1) {
			objX = objX - testStep;
		}
		glutPostRedisplay();
		break;

	case 'w':
		if (keyMode == 0) {
			if (testAngle2 >= 360)testAngle2 = 0;
			else
				testAngle2 = testAngle2 + 10;
		}
		else if (keyMode == 1) {
			objZ = objZ + testStep;
		}
		glutPostRedisplay();
		break;
	case 's':
		if (keyMode == 0) {
		if (testAngle2 <= 0)testAngle2 = 360;
		else
			testAngle2 = testAngle2 - 10;
	}
		else if (keyMode == 1) {
			objZ = objZ - testStep;
		}
		glutPostRedisplay();
		break;
	case 'f':
		if (keyMode == 0) {
			if (testX <= 40)testX += 0.5;
		}
			else if (keyMode == 1) {
				objY = objY + testStep;
			}
		glutPostRedisplay();
		break;
	case 'g':
		if (keyMode == 0) {
			if (testX >= 0.5)testX -= 0.5;
		
	}
			else if (keyMode == 1) {
				objY = objY - testStep;
			}
		glutPostRedisplay();
		break;
	case 'j':
		if (testY <= 40)testY += 0.5;
		glutPostRedisplay();
		break;
	case'k':
		if (testY >= -40)testY -= 0.5;
		glutPostRedisplay();
		break;
	case'1':
		if (flash)
			flash=false;
		else
			flash = true;
		glutPostRedisplay();
		break;
	case'2':
		if (moonLight)
			moonLight = false;
		else
			moonLight = true;
		glutPostRedisplay();
		break;


	default:
		break;
}

}

//levitate the key
void levitating(void) {
	key_y = 0.4 * sin(someT / 30);
	//cout << "sin(someT/360)= " << sin(someT ) << endl;
	someT = someT + 3;
	if (someT / 30 >= 360) someT = 0;
	glutPostRedisplay();
}

//i couldn't get this to work
//the original hit function works with ortho view, but for this hw
//i cant use that ortho view.
void findClosestHit(int hits, unsigned int buffer[])
{
	unsigned int *ptr, minZ;

	minZ = 0xffffffff; // 2^32 - 1
	ptr = buffer;
	closestName = 0;
	for (int i = 0; i < hits; i++)
	{
		ptr++;
		if (*ptr < minZ)
		{
			minZ = *ptr;
			ptr += 2;
			closestName = *ptr;
			ptr++;
		}
		else ptr += 3;
	}
} // end findClosestHit

void mouseControl(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		
		door_anim = true;
		key_idle = false;
		
		glutPostRedisplay();
		if (mouseX == 0 && mouseY == 0) {
			mouseX = x;
			mouseY = y;
		}
	}
}

void mouseMotion(int x, int y) {
	double xoff = fabs(mouseX - x);
	if (key_anim = true && xoff<720) {
		key_r_z = xoff*0.2;
		doorAngle = xoff *0.2;
	}
	
}
//key inputs for moving the player
void specialKeyInput(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP: //forward
		playerX = playerX + stepsize * sin(angle*PI / 180);
		playerZ = playerZ + stepsize * cos(angle*PI / 180);
		break;
	case GLUT_KEY_END:
		playerZ = playerZ + stepsize * sin(angle*PI / 180);
		playerX = playerX + stepsize * cos(angle*PI / 180);
		break;
	case GLUT_KEY_PAGE_DOWN:
		playerZ = playerZ - stepsize * sin(angle*PI / 180);
		playerX = playerX - stepsize * cos(angle*PI / 180);
		break;
	case GLUT_KEY_DOWN: //back

		playerX = playerX - stepsize * sin(angle*PI / 180);
		playerZ = playerZ - stepsize * cos(angle*PI / 180);
		break;
	case GLUT_KEY_RIGHT: //turn right
		angle = angle - turnsize;
		break;
	case GLUT_KEY_LEFT: //turn left
		angle = angle + turnsize;
		break;
	case GLUT_KEY_F1:
		cout << "current location:" << endl;
		cout << "x= " << playerX << endl;
		cout << "z= " << playerZ << endl;
		cout << "test x=" << testX << endl;
		cout << "test y=" << testY << endl;
		
		break;
	case GLUT_KEY_F2:
		if (keyMode == 0) {
		cout << "switched to movement mode" << endl;
		keyMode = 1;
	}
		else {
			cout << "switched to turn mode" << endl;
			keyMode = 0;
		}
		break;
		//helps identify locations
	case GLUT_KEY_F3:
		cout << "object location: " << endl;
		cout << "x= " << objX << endl;
		cout << "y= " << objY << endl;
		cout << "z= " << objZ << endl;
		cout << "object rotation: " << endl;
		cout << "Horrizontal angle= " << testAngle << endl;
		cout << "vertical angle= " << testAngle2 << endl;
		
		break;


	}//end switch
	glutPostRedisplay();
}
void drawSceneSmall(){
	Painter first;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//light pos is where player is at
	float lightPos[] = { playerX,playerY,playerZ, 1.0 }; // Spotlight position.
	float spotDirection[] = { sin(angle*PI / 180),	0.0,	
								cos(angle*PI / 180) }; // Spotlight direction.
	glLoadIdentity();
	//gluLookAt goes here
	gluLookAt(playerX, playerY, playerZ, playerX + sin(angle*PI / 180), playerY, playerZ + cos(angle*PI / 180), 0, 1, 0);
	
	
	drawTest();
	
	if(!moonLight)glDisable(GL_LIGHT1);
	else glEnable(GL_LIGHT1);
	//lights
	if (!flash)
	{
		glDisable(GL_LIGHT0);
	}
	else
	{
		glEnable(GL_LIGHT0);
	}
	//light pos is where player is at
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, spotAngle);
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spotDirection);
	glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, spotExponent);
	
	

	
	glutSwapBuffers();
}
int main(int argc, char **argv)
{
	printInteraction();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("hw04 testing");

	setup();
	glutDisplayFunc(drawSceneSmall);

	glutReshapeFunc(resize);
	glutKeyboardFunc(keyInput);
	glutSpecialFunc(specialKeyInput);
	glutMouseFunc(mouseControl);
	glutMotionFunc(mouseMotion);
	glutTimerFunc(5, animate, 1);
	glutMainLoop();

	return 0;
}