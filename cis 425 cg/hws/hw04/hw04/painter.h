//Xiaozhi Li
//my painter file, holds objects that I want to paint to the scene
//so they do not get compiled every time
//so I can organize them better.
//header reference:
// I used this tutorial from http://www.cplusplus.com/forum/articles/10627/
// I used it to construct my header file, I did not copy the code.
#ifndef __PAINTER_H_INCLUDED__   // if x.h hasn't been included yet...
#define __PAINTER_H_INCLUDED__
#include <cstdlib>
#include <vector>
//includes any dependencies.
#include <iostream>
#include <cmath>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

//
using namespace std;

class Painter {
public:
	int id = 0;
	void paintIt();
	void drawBox();
	void drawKey();
	void drawHouse();
	void drawFloor();
	void drawFlower(GLdouble x, GLdouble y, GLdouble z);
	void drawPetal(GLdouble angle);
	void drawTable();
	void drawPistril();
	void drawMoon(GLdouble r);
	void drawDoor();
	void drawWall(GLdouble width, GLdouble height, GLdouble length);
	void drawWallwithDoor(GLdouble width, GLdouble height, GLdouble length,GLdouble doorHeight, GLdouble doorWidth);
	//constructor
	Painter() {
		id = 1;
	}
};
void Painter::paintIt() {

	glPushMatrix();
	////////
	glTranslatef(-8, -3.5, 20);
	glutSolidCube(5);
	glPopMatrix();
	cout << "plz do something." << endl;
}

//chest
void Painter::drawBox() {
	glColor3f(0.482, 0.408, 0.933);
	//i can use draw wall again
	GLdouble width = 0.3;
	GLdouble height = 0.3 ;
	GLdouble length = 0.02;
	
	glPushMatrix();

		//first wall
		drawWall(width, height, length);
		//second wall
		glPushMatrix();
			glTranslatef(-(width + length) / 2, 0, -width / 2);
			glRotated(90, 0, 1, 0);
			drawWall(width, height, length);
		glPopMatrix();
		//third wall
		glPushMatrix();
			glTranslatef((width + length) / 2, 0, -width / 2);
			glRotated(90, 0, 1, 0);
			drawWall(width, height, length);
		glPopMatrix();

		//fourth wall
		glPushMatrix();
			glTranslatef(0, 0, -width);
			drawWall(width, height, length);
		glPopMatrix();

		//roof
		glPushMatrix();
			glTranslatef(0, height / 2, -width / 2);
			glRotated(90, 1, 0, 0);
			glColor3f(0.863, 0.078, 0.235);
			drawWall(width*0.2, width*0.1, length);
			glColor3f(0.482, 0.408, 0.933);
			drawWall(width, width, length);
		glPopMatrix();
		//floor
		glPushMatrix();
			glTranslatef(0, -height / 2, -width / 2);
			glRotated(90, 1, 0, 0);
			drawWall(width, width, length);
		glPopMatrix();
	glPopMatrix();
	
}

//draw my key
void Painter::drawKey() {
	double ringRadius = 3;
	double ringInner = 0.5;
	double keyEdgeLength = ringRadius*0.618*4;
	GLUquadricObj *quadratic;
	quadratic = gluNewQuadric();

	glPushMatrix();//begin key
	glScalef(.1, .1, .1);
	glColor3f(1.000, 0.843, 0.000);
	glEnable(GL_NORMALIZE);
	
	//key ring
	glutSolidTorus(ringInner, ringRadius, 40, 40);

	glPushMatrix();	//key main cylinder
	
	glColor3f(1.000, 0.843, 0.000);
	glTranslatef(0, -ringInner-ringRadius, 0);
	glScalef(1, 1, 1);
	glRotatef(90, 1, 0, 0);
	gluCylinder(quadratic, ringInner, ringInner, keyEdgeLength, 15, 15);
	glPopMatrix(); //key main cylinder

	glPushMatrix();
	glTranslatef(ringInner, -keyEdgeLength + ringInner, 0);
	glRotatef(90, 0, 1, 0);
	gluCylinder(quadratic, ringInner, ringInner, keyEdgeLength*.2, 15, 15);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(ringInner, -(keyEdgeLength + ringRadius)*.92, 0);
	glRotatef(90, 0, 1, 0);
	gluCylinder(quadratic, ringInner, ringInner, keyEdgeLength*.32, 15, 15);
	glPopMatrix();
	
	glPopMatrix();//end key
	glColor3f(0, 0, 0);
	glDisable(GL_NORMALIZE);
}

//draw my House
void Painter :: drawHouse() {
	//house are made of walls

	GLdouble width = 8;
	GLdouble height = 9 * 0.618;
	GLdouble length = 0.1;
	GLdouble doorheight=3;
		GLdouble doorwidth=1.2;
	
	glPushMatrix();
		glRotatef(180, 0, 1, 0);
		glColor3f(0.294, 0.000, 0.510);
		//first wall
		drawWallwithDoor(width, height,length,doorheight,doorwidth);
		//second wall
		glPushMatrix();
			glTranslatef(-(width+length) / 2, 0, -width/2);
			glRotated(90, 0, 1, 0);
			drawWall(width, height, length);
		glPopMatrix();
		//third wall
		glPushMatrix();
			glTranslatef((width + length) / 2, 0, -width / 2);
			glRotated(90, 0, 1, 0);
			//make a window here.
			GLdouble windowH = 2.2;
			GLdouble windowW = 2.2;
			GLdouble hWall = (height - windowH) / 2;
			GLdouble wWall = (width - windowW) / 2;
			//up
			glPushMatrix();
			glTranslatef(0, (height - hWall) / 2, 0);
			drawWall(width, hWall, length);
			glPopMatrix();
			//down
			glPushMatrix();
			glTranslatef(0, -(height - hWall) / 2, 0);
			drawWall(width, hWall, length);
			glPopMatrix();
			//left
			glPushMatrix();
			glTranslatef(-(width - wWall) / 2, 0, 0);
			drawWall(wWall, windowH, length);
			glPopMatrix();
			//right
			glPushMatrix();
			glTranslatef((width - wWall) / 2, 0, 0);
			drawWall(wWall, windowH, length);
			glPopMatrix();
		glPopMatrix();

		//fourth wall
		glPushMatrix();
			glTranslatef(0, 0, -width );
			drawWall(width, height, length);
		glPopMatrix();

		//roof
		glPushMatrix();
			glTranslatef(0, height/2,-width/2 );
			glRotated(90, 1, 0, 0);
			drawWall(width, width, length);
		glPopMatrix();
		//floor
		glPushMatrix();
			glTranslatef(0, -height / 2, -width / 2);
			glRotated(90, 1, 0, 0);
			drawWall(width, width, length);
		glPopMatrix();
	glPopMatrix();
		

	
}
//draw wall of width x height y
void Painter::drawWall(GLdouble width, GLdouble height, GLdouble length) {
	width = fabs(width);
	height = fabs(height);
	length = fabs(length);
	if (length == 0) length = 1;
	glEnable(GL_NORMALIZE);
	
	//glScalef(width,height, length);
	int j = 0;
	int i = 0;
	for (j = 0; j < width*10; j++)
	{
		for (i = 0; i < height*10; i++)
		{
			glPushMatrix();
			glTranslatef(width/2 - 0.1*j, height/2 - 0.1* i, 0);
			//glRotatef(-180, 0, 1, 0);
			glScalef(1.2,1.2,length*12);
			glutSolidCube(0.1);
			
			glPopMatrix();

		}
		
	}
	glDisable(GL_NORMALIZE);
	//walls are made of cubes

}
void Painter::drawWallwithDoor(GLdouble width, GLdouble height, GLdouble length, GLdouble doorHeight, GLdouble doorWidth) {
	width = fabs(width);
	height = fabs(height);
	length = fabs(length);
	doorHeight = fabs(doorHeight);
	doorWidth = fabs(doorWidth);
	if (length == 0) length = 1;
	if(doorHeight==0) doorHeight = 3;
	if(doorWidth==0) doorWidth = 2;

	
	glPushMatrix();
	//the upper part of the wall
	glPushMatrix();
	glTranslatef(0, doorHeight/2, 0);
	drawWall(width, height - doorHeight, length);
	glPopMatrix();
	//the bottom left part
	glPushMatrix();
	glTranslatef(-doorWidth*1.3*.5, (doorHeight-height)/2, 0);
	drawWall(width-(doorWidth*1.3), doorHeight, length);
	glPopMatrix();
	//the bottom right part
	glPushMatrix();
	glTranslatef((width - (doorWidth*.3) ) / 2, (doorHeight - height) / 2, 0);
	drawWall(doorWidth*.3, doorHeight, length);
	glPopMatrix();
	glPopMatrix();
	
}

void Painter::drawFloor() {
	glPushMatrix();
	glEnable(GL_NORMALIZE);
	glColor3f(0.412, 0.412, 0.412);
	glTranslatef(-8, -2.5, 20);
	glScalef(300, 0.1, 300);
	glutSolidCube(1);
	glDisable(GL_NORMALIZE);
	glPopMatrix();
}

void Painter::drawTable() {

}


//my draw flower from hw3
void Painter::drawFlower(GLdouble x, GLdouble y, GLdouble z) {
	//draw petal
	glEnable(GL_NORMALIZE);
	glPushMatrix();
	glTranslatef(x, y, z);
	glScalef(0.05, 0.05, 0.05);
	glPushMatrix();
	drawPetal(90);
	drawPetal(0);
	drawPetal(270);
	drawPetal(180);
	drawPistril();
	glScaled(0.05, 1.5, 0.05);
	glColor3f(0.0, 0.8, 0.0);
	glutSolidCube(2);
	glPopMatrix();
	glPopMatrix();
	glDisable(GL_NORMALIZE);
}
//sub of drawFlower
void Painter::drawPetal(GLdouble angle) {
	glPushMatrix();
	glRotated(angle, 0, 1, 0);
	glTranslated(-0.22, 1.55, 0.0);
	glEnable(GL_CLIP_PLANE0);
	double clip0[4] = { -1, 0.0, 0.0, 0.0 };
	glClipPlane(GL_CLIP_PLANE0, clip0);
	glScaled(1.2, 0.1, 0.2);
	glColor3f(0.9, 0.9, 0.0);
	
		glColor3f(.7, .6, .3);
		glutSolidCube(2.3);

	glPopMatrix();
	glDisable(GL_CLIP_PLANE0);
}
//sub of drawFlower
void Painter::drawPistril() {
	glPushMatrix();
	glTranslated(0.0, 1.5, 0.0);
	glEnable(GL_CLIP_PLANE0);
	double clip0[4] = { 0.0, -1, 0.0, 0.0 };
	glClipPlane(GL_CLIP_PLANE0, clip0);
	glColor3f(0.75, 0.0, 0.0);
	glutWireSphere(.3, 15, 15);
	glDisable(GL_CLIP_PLANE0);
	glPopMatrix();
}

void Painter::drawDoor() {
	glPushMatrix();
	glColor3f(0.741, 0.718, 0.420);
	glTranslatef(-0.6, 0, 0);
	glRotatef(90, 0, 0, 1);
	drawWall(3, 1.2, 0.1);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1.12, 0, -0.1);
	glColor3f(1, 0, 0);
	glutSolidCube(0.05);
	glPopMatrix();
}
void Painter::drawMoon(GLdouble r) {
	glEnable(GL_NORMALIZE);
	glColor3f(1.000, 0.843, 0.000);
	glutSolidSphere(r, 30, 30);
	glDisable(GL_NORMALIZE);
}
#endif