//
//  walkingAround.cpp
//   Marjory Baruch
//  builds a ring of numbers, corresponding
//  to angles in degrees, in a circle of radius
//  50, centered at the origin.

//  This version has no lookat.

//  floor is xz plane, positive y is up.

#include <iostream>
#include <cmath>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

#define PI 3.14159
using namespace std;

static int isSelecting = 0; // In selection mode?
static int hits; // Number of entries in hit buffer.
static unsigned int buffer[1024]; // Hit buffer.
static unsigned int closestName = 0; // Name of closest hit.
float speed = 0.5;
static int isAnimate = 1; // Animated?
static int animationPeriod = 30; // Time interval between frames.
static float spotExponent = 10.0; // Spotlight exponent = attenuation.
static float spotAngle = 50.0; // Spotlight cone half-angle.
							   //I don't use these by you can
static float meX = 0, meY = 0, meZ = 0;
static float angle = 0;
static float stepsize = 2.0;  //step size
static float turnsize = 10.0; //degrees to turn
							  //ghost position
static float gX = 0;
static float gY = 0;
static float gZ = 0;
//ghost trans
static float gtX, gtY, gtZ = 0;
//ghost angles
static float gAngle = 0;
static float gFaceAngle = 0;
//ghost radius
static float gR = 27;
static float isFaceRot = 1;
//key
static float moveKey = 0;
static float keyIn = 0;
static float kX, kY, kZ = 0;
//head light
static int headLight = 0;
//ceiling light
static int switchOff = 0;
//lock
static int openLock = 0;
static int rightK = 0;
static int leftK = 0;
// Initialization routine.
void setup(void)
{
	//glClearColor(1.0, 1.0, 1.0, 0.0);
	glEnable(GL_DEPTH_TEST); // Enable depth testing.


	glClearColor(0.0, 0.0, 0.0, 0.0);


	// Turn on OpenGL lighting.
	glEnable(GL_LIGHTING);

	// Light property vectors.
	float lightAmb[] = { 0.0, 0.0, 0.0, 1.0 };
	float lightDif[] = { 1, 1, 1, 1.0 };
	float lightSpec[] = { 1,1,1, 1.0 };
	float globAmb[] = { 0.1, 0.1, 0.1, 1.0 };


	float lightAmb2[] = { 0.0, 0.0, 0.0, 1.0 };
	float lightDif2[] = { 0.5, 0.5, 0.5, 1.0 };
	float lightSpec2[] = { 0.5,0.5,0.5, 1.0 };
	float lightPos2[] = { 0.0, 4, 30, 1.0 };


	// HeadLight properties.
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmb);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDif);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpec);

	// ceiling light properties
	glLightfv(GL_LIGHT1, GL_AMBIENT, lightAmb2);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightDif2);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lightSpec2);
	glLightfv(GL_LIGHT1, GL_POSITION, lightPos2);
	glEnable(GL_LIGHT1);



	glEnable(GL_LIGHT0); // Enable particular light source.
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globAmb); // Global ambient light.
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE); // Enable local viewpoint.

														 // Material property vectors.
	float matSpec[] = { 0.5,0.5,0.5,1 };

	float matShine[] = { 50.0 };

	// Material properties shared by all the spheres.
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpec);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, matShine);

	// Cull back faces.
	//////glEnable(GL_CULL_FACE);
	//////glCullFace(GL_BACK);

	// Enable color material mode:
	// The ambient and diffuse color of the front faces will track the color set by glColor().
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);






}

// OpenGL window reshape routine.
void resize(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(120, 1, 1, 100);
	glMatrixMode(GL_MODELVIEW);
}

// Routine to draw a stroke character string.
void writeStrokeString(void *font, const char *string)
{
	const char *c;
	for (c = string; *c != '\0'; c++) glutStrokeCharacter(font, *c);
}


// Drawing routine.



void rotGhostFace(void)
{
	if (isFaceRot == 1)
	{
		gFaceAngle += 10.0;
		if (gFaceAngle > 360.0) gFaceAngle -= 360.0;
		///cout << gFaceAngle << endl;

		glutPostRedisplay();
	}
}


void faceRot(void)
{
	if (isFaceRot == 1)
	{
		glutIdleFunc(rotGhostFace);
	}
}

void animate(int value)
{
	if (isAnimate)
	{
		gAngle += 0.1;

		if (gAngle == 360) gAngle -= 360;


		if (moveKey)
		{
			if (kX > -17.5)
			{
				kX -= 0.5;
			}
			else if (kZ < 7)
			{
				kZ += 0.5;
			}
			else if (kY > -2.5)
			{
				kY -= 0.3;
			}
			else
			{
				keyIn = 1;
			}
		}

		if (keyIn)
		{
			if (rightK)
			{
				openLock = 1;
			}
		}

	}


	glutTimerFunc(animationPeriod, animate, 1);
	glutPostRedisplay();

}

void drawBox(void)
{
	glEnable(GL_NORMALIZE);
	glPushMatrix();
	////////
	glTranslatef(-18, -3.5, 20);
	glScalef(0.7, 0.7, 0.7);
	glPushMatrix();
	glColor3f(0.2, 0.5, 0.6);
	//glTranslatef(3, -1, 15);
	glScalef(1, 1, 0.1);
	glutSolidCube(3.0);
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.2, 0.5, 0.6);
	glTranslatef(0, 0, 3);
	glScalef(1, 1, 0.1);
	glutSolidCube(3.0);
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.2, 0.5, 0.6);
	glTranslatef(1.5, 0, 1.5);
	glScalef(0.1, 1, 1);
	glutSolidCube(3.0);
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.2, 0.5, 0.6);
	glTranslatef(-1.5, 0, 1.5);
	glScalef(0.1, 1, 1);
	glutSolidCube(3.0);
	glPopMatrix();

	///top
	glPushMatrix();
	if (openLock)
	{
		glTranslatef(-2, 2, 0);
		glRotatef(90, 0, 0, -1);
	}
	glColor3f(0.2, 0.5, 0.3);
	glTranslatef(0, 1.5, 1.5);
	glScalef(1, 0.1, 1);
	glutSolidCube(3.0);
	glPopMatrix();

	//lock
	glPushMatrix();
	glColor3f(1, 1, 0.0);
	glTranslatef(1.8, 0.5, 1.5);
	glScalef(0.1, 0.2, 0.4);
	glutSolidCube(2.0);
	glPopMatrix();
	////////
	glPopMatrix();
	glColor3f(0, 0, 0);








	glDisable(GL_NORMALIZE);

}


void drawGhost(void)
{
	glPushMatrix();


	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_LIGHTING);
	glTranslatef(0, 0, 25);
	glTranslatef(gR*cos(gAngle) + gtX, gtY, gR*sin(gAngle) + gtZ);
	///cout << gAngle << endl;
	glPushMatrix();
	glRotatef(gFaceAngle, 0, 0, 1);
	glBegin(GL_POLYGON);
	glColor3f(1, 1, 1);
	int j = 0;
	for (j = 0; j <= 20 * PI; j += 1)
	{
		glVertex3f(1 * (cos(j)) + gX, gY + 1 * (sin(j)), gZ);
		glVertex3f(1 * (cos(j)) + gX, gY + 1 * (sin(j)), gZ);
	}
	glEnd();
	glPopMatrix();

	glLineWidth(2.0);
	glColor3f(0, 0, 0);
	glPushMatrix();
	glBegin(GL_LINES);
	glVertex3f(0.5, 0.1, -0.01);
	glVertex3f(0.3, 0.1, -0.01);
	///
	glVertex3f(0.5 - 1, 0.1, -0.01);
	glVertex3f(0.3 - 1, 0.1, -0.01);
	///
	glVertex3f(0.5 - 0.5, 0.1, -0.01);
	glVertex3f(0.5 - 0.5, 0.1 - 0.3, -0.01);
	///
	glLineWidth(5.0);
	glVertex3f(0.3, 0.1 - 0.5, -0.01);
	glVertex3f(-0.3, 0.1 - 0.5, -0.01);
	glEnd();
	glPopMatrix();

	glEnable(GL_LIGHTING);





	glColor3f(0, 0, 0);
	glPopMatrix();
}


void drawTable(void)
{
	glEnable(GL_NORMALIZE);
	//table
	glPushMatrix();
	glColor3f(0.7, 0.3, 0);
	glTranslatef(3, -1, 15);
	glScalef(1, 0.1, 1);
	glutSolidCube(5.0);
	glPopMatrix();
	//leg1
	glPushMatrix();
	glTranslatef(5, -3.5, 13);
	glScalef(0.05, 1, 0.05);
	glutSolidCube(5.0);
	glPopMatrix();
	//leg2
	glPushMatrix();
	glTranslatef(5, -3.5, 17);
	glScalef(0.05, 1, 0.05);
	glutSolidCube(5.0);
	glPopMatrix();
	//leg3
	glPushMatrix();
	glTranslatef(1, -3.5, 13);
	glScalef(0.05, 1, 0.05);
	glutSolidCube(5.0);
	glPopMatrix();
	//leg4
	glPushMatrix();
	glTranslatef(1, -3.5, 17);
	glScalef(0.05, 1, 0.05);
	glutSolidCube(5.0);
	glPopMatrix();

	glColor3f(0, 0, 0);
	glDisable(GL_NORMALIZE);
}


void drawPot(void)
{
	float matShine2[] = { 100.0 };
	float matSpec2[] = { 1.0, 1.0, 1.0, 1.0 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matSpec2);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, matShine2);
	glColor4f(0.5, 0.3, 0.9, 1.0);
	glPushMatrix();
	glTranslatef(1.2, 0, 16);
	glutSolidTeapot(1.0);
	glPopMatrix();
}

void drawKey(void)
{
	glEnable(GL_NORMALIZE);
	glPushMatrix();
	glTranslatef(1.2 + kX, -0.5 + kY, 14 + kZ);
	glColor3f(1, 1, 0);
	glPushMatrix();
	//glTranslatef(1.2, 0, 16);
	glScalef(1, 0.1, 0.1);
	glutSolidCube(1.0);
	glPopMatrix();


	glPushMatrix();
	glTranslatef(-0.3, 0.1, 0);
	//glTranslatef(1.2, 0, 16);
	glScalef(0.1, 0.1, 0.1);
	glutSolidCube(1.0);
	glPopMatrix();


	glColor3f(0.8, 0.2, 0.3);

	glPushMatrix();
	glTranslatef(0.4, 0, 0);
	//glTranslatef(1.2, 0, 16);
	//glScalef(0.1, 0.1, 0.1);
	glutSolidSphere(0.2, 20, 20);
	glPopMatrix();


	glPopMatrix();
	glColor3f(0, 0, 0);
	glDisable(GL_NORMALIZE);
}


void drawSwitch(void)
{
	glColor3f(0.9, 0.9, 0.9);
	glEnable(GL_NORMALIZE);
	glPushMatrix();
	glTranslatef(0.4, 0, 0);
	glTranslatef(2, 0, 34);
	glScalef(0.2, 0.4, 0.1);
	glutSolidCube(2);
	///glutSolidSphere(0.2, 20, 20);
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.9, 0, 0);
	glTranslatef(2.4, 0, 34);
	glutSolidSphere(0.2, 10, 10);
	glPopMatrix();

	glDisable(GL_NORMALIZE);
	glColor3f(0, 0, 0);
}


void drawWalls(void)
{
	//Walls
	glEnable(GL_NORMALIZE);
	////1
	int j = 0;
	int i = 0;
	for (j = 0; j < 120; j++)
	{
		for (i = 0; i < 360; i++)
		{
			glPushMatrix();
			glColor3f(1, 0, 0);
			glTranslatef(10 - 0.1*j, 18 - 0.1* i, 10);
			glRotatef(-180, 0, 1, 0);
			glScalef(2, 6, 0.5);
			glutSolidCube(0.3);
			glColor3f(0, 0, 0);
			glPopMatrix();

		}
	}
	////////////////

	for (j = 0; j < 120; j++)
	{
		for (i = 0; i < 100; i++)
		{
			glPushMatrix();
			glColor3f(1, 0, 0);
			glTranslatef(0 - 0.1*j, 18 - 0.1* i, 10);
			glRotatef(-180, 0, 1, 0);
			glScalef(2, 6, 0.5);
			glutSolidCube(0.3);
			glColor3f(0, 0, 0);
			glPopMatrix();

		}
	}


	//////

	for (j = 0; j < 120; j++)
	{
		for (i = 0; i < 360; i++)
		{
			glPushMatrix();
			glColor3f(1, 0, 0);
			glTranslatef(-8 - 0.1*j, 18 - 0.1* i, 10);
			glRotatef(-180, 0, 1, 0);
			glScalef(2, 6, 0.5);
			glutSolidCube(0.3);
			glColor3f(0, 0, 0);
			glPopMatrix();

		}
	}



	///////



	////2

	for (j = 0; j < 10; j++)
	{
		for (i = 0; i < 6; i++)
		{
			glPushMatrix();
			glColor3f(0, 0, 1);
			glTranslatef(10, 16 - 4 * i, 11 + 2.5*j);
			glRotatef(90, 0, 1, 0);
			glRotatef(-180, 0, 1, 0);
			glScalef(3, 3, 0.2);
			glutSolidCube(1);
			glColor3f(0, 0, 0);
			glPopMatrix();
		}
	}

	////3
	glPushMatrix();
	glColor3f(0, 1, 0);
	for (j = 0; j < 10; j++)
	{
		for (i = 0; i < 6; i++)
		{
			glPushMatrix();
			glColor3f(0, 0, 1);
			glTranslatef(-20, 16 - 4 * i, 11 + 2.5*j);
			glRotatef(90, 0, 1, 0);
			glRotatef(-180, 0, 1, 0);
			glScalef(3, 3, 0.2);
			glutSolidCube(1);
			glColor3f(0, 0, 0);
			glPopMatrix();
		}
	}
	glPopMatrix();



	////4
	int k = 0;
	int l = 0;
	for (l = 0; l < 2; l++)
	{
		for (k = 0; k < 8; k++)
		{
			glPushMatrix();
			glColor3f(1, 0, 1);
			glTranslatef(8 - 5 * l, 15 - 5 * k, 35);
			glRotatef(-180, 0, 1, 0);
			glScalef(5, 5, 0.2);
			glutSolidCube(1.0);
			glColor3f(0, 0, 0);
			glPopMatrix();

		}
	}
	/////
	for (k = 0; k < 2; k++)
	{
		glPushMatrix();
		glColor3f(1, 0, 1);
		glTranslatef(5.5 - 5 * l, 12.5 - 15 * k, 35);
		glRotatef(-180, 0, 1, 0);
		glScalef(5, 5, 0.2);
		glutSolidCube(2.0);
		glColor3f(0, 0, 0);
		glPopMatrix();

	}
	/////

	for (l = 0; l < 2; l++)
	{
		for (k = 0; k < 8; k++)
		{
			glPushMatrix();
			glColor3f(1, 0, 1);
			glTranslatef(-12.5 - 5 * l, 15 - 5 * k, 35);
			glRotatef(-180, 0, 1, 0);
			glScalef(5, 5, 0.2);
			glutSolidCube(1.2);
			glColor3f(0, 0, 0);
			glPopMatrix();

		}
	}

	//////roof

	glPushMatrix();
	glColor3f(0.5, 0.5, 0);
	glTranslatef(-5, 18, 23);

	glScalef(5, 0.1, 5);
	glutSolidCube(6);
	glColor3f(0, 0, 0);
	glPopMatrix();



	glDisable(GL_NORMALIZE);
}

void drawGround(void)
{
	//ground
	glPushMatrix();
	glColor3f(1, 1, 0.7);
	glTranslatef(0, -6, 0);
	glRotatef(180, 1, 0, 0);
	glScalef(100, 0.5, 100);
	glutSolidCube(5.0);
	glColor3f(0, 0, 0);
	glPopMatrix();

}




void drawSceneSmall(void)
{

	//test light
	float lightPos[] = { meX,meY,meZ, 1.0 }; // Spotlight position.
											 //float spotDirection[] = { 2,2,5 }; // Spotlight direction.   
	float spotDirection[] = { sin(angle*PI / 180),     0.0,    cos(angle*PI / 180) }; // Spotlight direction.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	//gluLookAt goes here
	gluLookAt(meX, meY, meZ, meX + sin(angle*PI / 180), meY, meZ + cos(angle*PI / 180), 0, 1, 0);
	//print the numbers in a circle
	//radius 50
	//0 at (0,0,50)
	//90 at (50,0,0)

	if (switchOff)
	{
		glDisable(GL_LIGHT1);
	}
	//head light
	//float lightPos[] = { meX, meY+1, meZ, 1.0 }; // Spotlight position.
	//float spotDirection[] = { meX + sin(angle*PI / 180),meY+1,meZ + cos(angle*PI / 180) }; // Spotlight direction. 
	if (headLight == 0) glDisable(GL_LIGHT0);
	else glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, spotAngle);
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spotDirection);
	glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, spotExponent);






	drawWalls();
	drawTable();
	drawGround();
	drawGhost();
	faceRot();

	drawKey();
	drawSwitch();
	drawBox();
	drawPot();




	//test cube

	/////glEnable(GL_LIGHTING);
	glPushMatrix();
	glColor3f(1, 0, 0);
	glTranslatef(0, 0, -20);
	glutSolidCube(5.0);
	glColor3f(0, 0, 0);
	glPopMatrix();
	/////	glDisable(GL_LIGHTING);


	glColor4f(0.0, 0.0, 0.0, 1.0);
	/*
	for (int theta = 0; theta<360; theta += 10)
	{
	glPushMatrix();
	glTranslatef(50 * sin(theta*PI / 180), 0.0, 50 * cos(theta*PI / 180));
	glRotatef(theta, 0, 1, 0);

	glRotatef(180, 0.0, 1.0, 0.0); //line it up with 0 position
	glScalef(0.025, 0.025, 0.025);

	// convert a number to a string for printing
	char numString[5];
	sprintf_s(numString, "%d", theta);
	writeStrokeString(GLUT_STROKE_ROMAN, numString);

	glPopMatrix();
	}

	*/



	glutSwapBuffers();
}

void specialKeyInput(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP: //forward
		meX = meX + stepsize * sin(angle*PI / 180);
		meZ = meZ + stepsize * cos(angle*PI / 180);
		break;
	case GLUT_KEY_DOWN: //back
		meX = meX - stepsize * sin(angle*PI / 180);
		meZ = meZ - stepsize * cos(angle*PI / 180);
		break;
	case GLUT_KEY_RIGHT: //turn right
		angle = angle - turnsize;
		break;
	case GLUT_KEY_LEFT: //turn left
		angle = angle + turnsize;
		break;


	}//end switch
	glutPostRedisplay();
}

// Routine to output interaction instructions to the C++ window.
void printInteraction(void)
{
	cout << "Interaction:" << endl;
	cout << "l to turn on head light" << endl;
	cout << "\tup arrow - step forward" << endl;
	cout << "\tdown arrow - step backward" << endl;
	cout << "\tright arrow - turn to the right" << endl;
	cout << "\tleft arrow - turn to the left" << endl;
	cout << "\tr - reset to standing in the center ";
	cout << "facing 0 degrees." << endl;
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	case 'R':

		break;
	case 'l':
		if (headLight == 1) headLight = 0;
		else headLight = 1;
		break;
	case 'k':

		moveKey = 1;
		break;
	case 'L':
		if (switchOff == 0)
		{
			switchOff = 1;
			glDisable(GL_LIGHT1);
		}
		else
		{
			glEnable(GL_LIGHT1);
			switchOff = 0;
		}
		break;
	case 'e':
		if (keyIn)
		{
			rightK = 1;
		}
	case 'q':
		if (keyIn)
		{
			leftK = 1;
		}
		break;


	default:
		break;
	}

}


// Main routine.
int main(int argc, char **argv)
{
	printInteraction();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Walking around looking at numbers.");
	setup();
	glutDisplayFunc(drawSceneSmall);
	glutReshapeFunc(resize);
	glutTimerFunc(5, animate, 1);
	glutKeyboardFunc(keyInput);
	glutSpecialFunc(specialKeyInput);
	glutMainLoop();

	return 0;
}