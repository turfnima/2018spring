#CIS 400 social media & data mining
#Xiaozhi Li
# hw 01
#some code using python 60 lines of code
print "hello\
myfriend, welcome to our encryption/decryption\
system."
#public values
#offset
offset=6
#alphabet list
dic=['a','b','c','d','e','f','g',
         'h','i','j','k','l','m','n',
         'o','p','q','r','s','t','u',
         'v','w','x','y','z',' ']
class watup:
    def __init__(self,offset):
        self.o=offset
        #this list contains index of the list
        numb=[(x+offset)%27 for x in range(27)]
        #   offset the index from the list
        self.d2=[dic[x] for x in numb]

    def encrypt(self,input,encode): #n is string
        #for loop to encrypt each word
        x=len(input)
        #encryption
        #use a for loop to  convert every char to another char (offset)
        code=""
        for i in range(x):
            code=code+ encode[input[i]]
        return code
    def decrypt(self,input,decode):
        x=len(input)
        code=""
        for i in range(x):
            code=code+ decode[input[i]]
        return code
if __name__ == "__main__":
#if else statement
    try:
        myInput=raw_input("would you like to encrypt?\
                      type in 'y' or 'Y' to confirm\
                      anyother input will lead to decypt.\n")
    except ValueError:
        print"though unlikely, you still entered some \
        wrong value, we are existing"
        exit()
    if(myInput[0]=='y' or myInput[0]=='Y'):
        try:
            offset=int(raw_input("what offset we are using\
                                 for encryption?\n"))
        except ValueError:
            print "you had a wrong value, we are exiting"
            exit()
        #then encrypt
        try:
            myInput=raw_input("type in a message for encryption\n")
        except ValueError:
            print "please dont use keys that is beyond alphabet\n"
            print "we are existing\n"
            exit()
        obj=watup(offset)
        encode={dic[i]:obj.d2[i] for i in range(27)}
        print obj.encrypt(myInput,encode)
    else:
        try:
            offset=int(raw_input("what offset we are using for decryption?\n"))
        except ValueError:
            print "you had a wrong value, we are exiting"
            exit()
        #then encrypt
        try:
            myInput=raw_input("type in a message for decryption\n")
        except ValueError:
            print "please dont use keys that is beyond alphabet\n"
            print "we are existing\n"
            exit()
        obj=watup(offset)
        #decode dictionaries
        decode= {obj.d2[i]:dic[i] for i in range(27)}
        print obj.decrypt(myInput,decode)
