#Xiaozhi Li
import twitter
import sys
import json
import time
from urllib2 import URLError
from httplib import BadStatusLine
import networkx as nx
def oauth_login():
	CONSUMER_KEY = '3ZeWW7ICGq8JKG6uVsHyknpGt'
	CONSUMER_SECRET = 'oPbpqg3YnEEUilMy8oRNX6g3kM0gRtzBwFzhpZc81y863wH5yJ'
	OAUTH_TOKEN = '4168170197-snX1VLHl04arfNUdoE4ZcB6sww1NX2PvSaefcab'
	OAUTH_TOKEN_SECRET = 'nw0olWdDCqHGosANpMvcM8iYZjzhvlGmJFId7IKkunDDw'

	auth = twitter.OAuth(OAUTH_TOKEN, OAUTH_TOKEN_SECRET, CONSUMER_KEY, CONSUMER_SECRET)
	twitter_api = twitter.Twitter(auth=auth)
	return twitter_api

#make twitter request
#this function is from twitter cook book
def make_twitter_request(twitter_api_func, max_errors=10, *args, **kw):

    # A nested helper function that handles common HTTPErrors. Return an updated
    # value for wait_period if the problem is a 500 level error. Block until the
    # rate limit is reset if it's a rate limiting issue (429 error). Returns None
    # for 401 and 404 errors, which requires special handling by the caller.
    def handle_twitter_http_error(e, wait_period=2, sleep_when_rate_limited=True):

        if wait_period > 3600: # Seconds
            print >> sys.stderr, 'Too many retries. Quitting.'
            raise e

        # See https://dev.twitter.com/docs/error-codes-responses for common codes

        if e.e.code == 401:
            print >> sys.stderr, 'Encountered 401 Error (Not Authorized)'
            return None
        elif e.e.code == 404:
            print >> sys.stderr, 'Encountered 404 Error (Not Found)'
            return None
        elif e.e.code == 429:
            print >> sys.stderr, 'Encountered 429 Error (Rate Limit Exceeded)'
            if sleep_when_rate_limited:
                print >> sys.stderr, "Retrying in 15 minutes...ZzZ..."
                sys.stderr.flush()
                time.sleep(60*15 + 5)
                print >> sys.stderr, '...ZzZ...Awake now and trying again.'
                return 2
            else:
                raise e # Caller must handle the rate limiting issue
        elif e.e.code in (500, 502, 503, 504):
            print >> sys.stderr, 'Encountered %i Error. Retrying in %i seconds' %                 (e.e.code, wait_period)
            time.sleep(wait_period)
            wait_period *= 1.5
            return wait_period
        else:
            raise e

    # End of nested helper function

    wait_period = 2
    error_count = 0

    while True:
        try:
            return twitter_api_func(*args, **kw)
        except twitter.api.TwitterHTTPError, e:
            error_count = 0
            wait_period = handle_twitter_http_error(e, wait_period)
            if wait_period is None:
                return
        except URLError, e:
            error_count += 1
            time.sleep(wait_period)
            wait_period *= 1.5
            print >> sys.stderr, "URLError encountered. Continuing."
            if error_count > max_errors:
                print >> sys.stderr, "Too many consecutive errors...bailing out."
                raise
        except BadStatusLine, e:
            error_count += 1
            time.sleep(wait_period)
            wait_period *= 1.5
            print >> sys.stderr, "BadStatusLine encountered. Continuing."
            if error_count > max_errors:
                print >> sys.stderr, "Too many consecutive errors...bailing out."
                raise

#user profile
#this function is from twitter cook book
def get_user_profile(twitter_api, screen_names=None, user_ids=None):

    # Must have either screen_name or user_id (logical xor)
    assert (screen_names != None) != (user_ids != None),     "Must have screen_names or user_ids, but not both"

    items_to_info = {}

    items = screen_names or user_ids

    while len(items) > 0:

        # Process 100 items at a time per the API specifications for /users/lookup.
        # See https://dev.twitter.com/docs/api/1.1/get/users/lookup for details.

        items_str = ','.join([str(item) for item in items[:100]])
        items = items[100:]

        if screen_names:
            response = make_twitter_request(twitter_api.users.lookup,
                                            screen_name=items_str)
        else: # user_ids
            response = make_twitter_request(twitter_api.users.lookup,
                                            user_id=items_str)

        for user_info in response:
            if screen_names:
                items_to_info[user_info['screen_name']] = user_info
            else: # user_ids
                items_to_info[user_info['id']] = user_info

    return items_to_info

def print_list_to_txt(items):
	out_put=open("testOut.txt","wb")
#	while len(items) > 0:
#		items_str = ','.join([str(item) for item in items[:100]])
#		items = items[100:]
	for x in items:
		out_put.write(str(x))
		out_put.write('\n')
	out_put.close()
	return

##anything below belongs to Xiaozhi Li
##Though networkx were obviously from google
def get_top_five(twitter_api,social_network,screen_name=None,user_ids=None):
	assert (screen_name != None) != (user_ids != None),     "Must have screen_names or user_ids, but not both"
	if(screen_name):
		response = make_twitter_request(twitter_api.friends.ids,
										screen_name=screen_name)
		#print twitter_api
		if(response != None):
			##friends
			friends = response["ids"]
			response = make_twitter_request(twitter_api.followers.ids,									screen_name=screen_name)
			##followers
			followers = response["ids"]
			reciprocal_friends = set(friends) & set(followers)

	else:
		#user_id
		response = make_twitter_request(twitter_api.friends.ids,
										user_id=user_ids,count=5000)
		##friends
		##I had to make this if statement
		'''sometime the response gets an None value'''
		if(response != None):
			friends = response["ids"]
			response = make_twitter_request(twitter_api.followers.ids,
											user_id=user_ids,count=5000)
			##followers
			followers = response["ids"]
			reciprocal_friends = set(friends) & set(followers)

	#a list of all the repeating reciprocal_friends
		k=[elem for elem in reciprocal_friends if (elem in social_network)]
	#remove them (this saves a lot of time, hypothetically)
		for i in range(len(k)):
			reciprocal_friends.remove(k[i])
	#a little messy, converting it to list
	user_profile=get_user_profile(twitter_api,user_ids=list(reciprocal_friends))
	#get the follower count and order it
	reciprocal_popular_data={ids: user_profile[ids]["followers_count"] for ids in list(reciprocal_friends)}
	#sorted returns an accending list, then we reverse it by [::-1]
	ordered_friends = sorted(reciprocal_popular_data, key=reciprocal_popular_data.get)[::-1] # get() returns the value for a key
	top_five=ordered_friends[0:5]

	#the following add it to the Graph
	if(screen_name):
		social_network.add_edges_from([(screen_name,y) for y in top_five])
	else:
		social_network.add_edges_from([(user_ids,y) for y in top_five])
	return top_five

#my precious
#crawler
def crawler(twitter_api,social_network,top_five,l):
	limit=100

	while(len(l)<limit):
		k=[]
		for i in range(len(top_five)):
		#	print ("for loop: ", i, "\n")
			k+=get_top_five(twitter_api=twitter_api,social_network=social_network,user_ids=top_five[i])
			#they could be repeating
			#converting it to set and then back to list,
			#so no repeating elements
			k=list(set(k))
		l+=k
		#they definitely are repeating
		#again the same method
		l=list(set(l));
		print ("l= ",l)
		crawler(twitter_api=twitter_api,social_network=social_network,top_five=k,l=l)

#initialize the graph
import matplotlib.pyplot as plt
social_network=nx.Graph()

##all the ordinary routines
twitter_api = oauth_login()
screen_name = 'KattyPerry4U'
top_five=get_top_five(twitter_api=twitter_api,social_network=social_network,screen_name=screen_name)
l=top_five
crawler(twitter_api=twitter_api,social_network=social_network,top_five=top_five,l=l)
print l
nx.draw(social_network)

#output:
out_put=open("testOut.txt","wb")
print("number of nodes: " + str(nx.number_of_nodes(social_network))+"\n")
print("number of edges: " + str(nx.number_of_edges(social_network))+"\n")
print(nx.average_shortest_path_length(social_network))
print(nx.diameter(social_network))
out_put.write("node: ")
out_put.write(str(social_network.node()))
out_put.write("\n")
out_put.write("edges: ")
out_put.write(str(social_network.edges()))
out_put.write("\n")
out_put.write("average distance: ")
out_put.write(str(nx.average_shortest_path_length(social_network)))
out_put.write("\n")
out_put.write("diameter: ")
out_put.write(str(nx.diameter(social_network)))
out_put.close()

plt.savefig("social_network")
plt.show()
