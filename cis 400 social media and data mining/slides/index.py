class Index(object):
    class Person(object):
        def __init__(self, name):
            self.name = name

    class PersonDetail(object):
        def __init__(self, name, profession):
            self.name = name
            self.profession = profession


    def CustomerName(self):
        people = [self.Person("Nick"), self.Person("Alice")]
        Name_List = [(p.name) for p in people]
        return Name_List

    def CustomerDetails(self, name):
        people = [self.PersonDetail("Nick", "Programmer"), self.PersonDetail("Alice", "Engineer")]
        professions = dict([(p.name, p.profession) for p in people])
        i =0
        while i < len(professions):
            if professions.keys()[i] == name:
                return name +" is a/an " + professions.values()[i]
                break
            else:
                i = i+1;
        return "No match found"

if __name__ == "__main__":
    obj = Index()
    try:
        user_input = int(raw_input("Press 0 to see all customer or press 1 to see specific customer details"))
    except StandardError, e:
        print "Error - ", e
    if user_input == 0:
        print obj.CustomerName()
    else:
        user_name = raw_input("Enter Customer Name")
        print obj.CustomerDetails(user_name)